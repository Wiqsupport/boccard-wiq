<?php
error_reporting(0);	// E_ALL
ini_set("display_errors", 0); // 1

// For 4.3.0 <= PHP <= 5.4.0
if (!function_exists('http_response_code')) {
    function http_response_code($newcode = NULL) {
        static $code = 200;
        if($newcode !== NULL)         {
            header('X-PHP-Response-Code: '.$newcode, true, $newcode);
            if(!headers_sent())
                $code = $newcode;
        }       
        return $code;
    }
}		

function Litl2Big($ch) {
	$ch = explode("���",chunk_split($ch,2,"���")); 
	$ch = array_reverse($ch);
	$ch = implode('',$ch);
	return($ch);
}

$ch = isset($_REQUEST['id']) ? explode("-", $_REQUEST['id']) : '';
if ( (count($ch)==2) && ( (hash('crc32b',hexdec($ch[1]))==$ch[0]) || (Litl2Big(hash('crc32b',hexdec($ch[1])))==$ch[0]) ) ) {
	$ret = array();

	// Infos G�n�rales
	$ret['general']['status'] = 'ok';
	$ret['general']['version'] = '2015.06.02';

	// Infos serveur
	$ret['serveur']['date'] = date("Y-m-d");
	$ret['serveur']['heure'] = date("H:i:s");
	$ret['serveur']['dec'] =  date("O");
	$ret['serveur']['fuseau'] =  date("T");
	$ret['serveur']['ete'] =  date("I");
	$ret['serveur']['host'] = $_SERVER['HTTP_HOST'];
	$ret['serveur']['user'] = $_SERVER['USER'];
	$ret['serveur']['remote_IP'] = $_SERVER['HTTP_REMOTE_IP'];
	$ret['serveur']['name'] = $_SERVER['SERVER_NAME'];
	$ret['serveur']['addr'] = $_SERVER['SERVER_ADDR'];
	$ret['serveur']['document_root'] = $_SERVER['DOCUMENT_ROOT'];

	// Infos PHP
	$infos = phpinfo_array();
	// print_r($infos);
	$ret['php']['version'] = isset($infos['Core']['PHP Version']) ? $infos['Core']['PHP Version'] : phpversion() ;
	$ret['php']['system'] = $infos[phpinfo][System];
	$ret['php']['ini'] = $infos[phpinfo]['Loaded Configuration File'];

	// Infos Wordpress
	if (include($_SERVER['DOCUMENT_ROOT'].'/wp-blog-header.php')) {
		$ret['general']['cms'] = "WP";
		$ret['wp']['version'] = $wp_version;
		$ret['wp']['bdversion'] = $wp_db_version;
		$ret['wp']['updates'] = Maj_WP();	// "updates":{"plugins":0,"themes":0,"wordpress":0,"translations":0}
	
		// $update_plugins = get_site_transient( 'update_plugins' );
		// if ( ! empty( $update_plugins->response ) )
			// $ret['wp']['plugins'] = count( $update_plugins->response );
			
		// Info plugin Makheia
		$ret['wp']['makheia'] = (isset($inst_makheia)) ? ($inst_makheia->version) : 0;
	} else {
		$ret['wp']['version'] = 0;
	}
	
	// Infos Joomla	
	define('_JEXEC', 1);
	define('DS', DIRECTORY_SEPARATOR);
	if (file_exists(dirname(__FILE__) . '/defines.php')) {
		include dirname(__FILE__) . '/defines.php';
	}
	if (!defined('_JDEFINES')) {
		define('JPATH_BASE', dirname(__FILE__));
		if (file_exists(JPATH_BASE.DS.'includes'.DS.'defines.php'))
			include JPATH_BASE.DS.'includes'.DS.'defines.php';
	}	
	if (defined('JPATH_LIBRARIES')) {
		$ret['general']['cms'] = "JM";
		if (file_exists(JPATH_LIBRARIES.DS.'cms'.DS.'version'.DS.'version.php')) {
			define('JPATH_PLATFORM',1);
			include JPATH_LIBRARIES.DS.'cms'.DS.'version'.DS.'version.php';
		} elseif (file_exists(JPATH_LIBRARIES.DS.'joomla'.DS.'version.php')) {
			include JPATH_LIBRARIES.DS.'joomla'.DS.'version.php';
		}
		if (class_exists("JVersion")) {
			$instance = new JVersion();
			$ret['joom']['version'] = $instance->RELEASE.'.'.$instance->DEV_LEVEL;
		}
	}
		
	echo (json_encode($ret));	
} else {
	http_response_code(400);
	// header("HTTP/1.0 404 Not Found");
}


function phpinfo_array() {
	ob_start();
	phpinfo();
	$phpinfo = array('phpinfo' => array());
	if(preg_match_all('#(?:<h2>(?:<a name=".*?">)?(.*?)(?:</a>)?</h2>)|(?:<tr(?: class=".*?")?><t[hd](?: class=".*?")?>(.*?)\s*</t[hd]>(?:<t[hd](?: class=".*?")?>(.*?)\s*</t[hd]>(?:<t[hd](?: class=".*?")?>(.*?)\s*</t[hd]>)?)?</tr>)#s', ob_get_clean(), $matches, PREG_SET_ORDER))
	foreach($matches as $match)
		if(strlen($match[1]))
			$phpinfo[$match[1]] = array();
		elseif(isset($match[3]))
			$phpinfo[end(array_keys($phpinfo))][$match[2]] = isset($match[4]) ? array($match[3], $match[4]) : $match[3];
		else
			$phpinfo[end(array_keys($phpinfo))][] = $match[2];
	return $phpinfo;
}


function Maj_WP() {
	// � partir du code de  wp_get_update_data

	$counts = array( 'plugins' => 0, 'themes' => 0, 'wordpress' => 0, 'translations' => 0 );

	$update_plugins = get_site_transient( 'update_plugins' );
	if ( ! empty( $update_plugins->response ) )
		$counts['plugins'] = count( $update_plugins->response );
		
	$update_themes = get_site_transient( 'update_themes' );
	if ( ! empty( $update_themes->response ) )
		$counts['themes'] = count( $update_themes->response );
		
	$from_api = get_site_transient( 'update_core' );
	if ( ! isset( $from_api->updates ) || ! is_array( $from_api->updates ) )
		$counts['wordpress']=0;
	else {
		$updates = $from_api->updates;
		$counts['wordpress']=max((count($updates)-1),0);
	}

	// if ( ( $core || $plugins || $themes ) && wp_get_translation_updates() )
	if ( wp_get_translation_updates() )
		$counts['translations'] = 1;

	$counts['total'] = $counts['plugins'] + $counts['themes'] + $counts['wordpress'] + $counts['translations'];

	return $counts;
}

?>
