<div id="template_DNA">
<?php
global $language;
$lang = $language->language;
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes;?>">
    <div class="node-inner">

        <div class="banner relative hidden-xs">
        <?php print theme('image_style', array(
          'style_name' => 'banner', 
          'path' => $node->field_image['und'][0]['uri'],
          'alt' => $node->field_image['und'][0]['alt'] ,
          'title' => $node->field_image['und'][0]['title']
            ));?>
            <div class="absolute absolute-dropdown_title_brief">
                <h2 class="title_sml"><?php print t("Boccard's DNA"); ?></h2>
                <h1 class="title"><?php print $node->title; ?></h1>
                <div class="brief"><?php print isset($node->field_brief['und'][0]['value']) ? $node->field_brief['und'][0]['value'] : ""; ?></div>
            </div>
        </div>
        
        <div class="dna_banner_mobile">
            <div class="img"><?php print theme('image_style', array(
              'style_name' => 'banner', 
              'path' => $node->field_image['und'][0]['uri'],
              'alt' => $node->field_image['und'][0]['alt'] ,
              'title' => $node->field_image['und'][0]['title']
                )) ?></div>
            <div class="mobile_grey">
              <h2 class="title_sml"><?php print t("Boccard's DNA"); ?></h2>
              <h1 class="title"><?php print $node->title; ?></h1>
              <div class="brief"><?php print isset($node->field_brief['und'][0]['value']) ? $node->field_brief['und'][0]['value'] : ""; ?></div>
            </div>
        </div>
        
        
        
        <?php 
        $transid= translation_node_get_translations($node->nid);
        
        if($node->nid==606 || $node->nid==851|| $node->nid==1471 || $node->nid==1622 || $node->nid==2670 || $node->nid==3381){?>
        <div class="under_banner_grey relative">
            <div class="rounded absolute">
              <?php print theme('image_style', 
                  array(
                      'style_name' => 'rounded', 
                      'path' => $node->field_rounded_image['und'][0]['uri'],
                      'alt' => $node->field_rounded_image['und'][0]['alt'] ,
                      'title' => $node->field_rounded_image['und'][0]['title']
                  )
                  );
              ?></div>
            <div class="names_positions absolute">
                <div class="names"><?php print $node->field_names['und'][0]['value']; ?></div>
                <div class="positions"><?php print isset($node->field_position['und'][0]['value']) ? $node->field_position['und'][0]['value'] : ""; ?></div>
        </div>
        </div>
        <?php }?>
        
        <div class="title_paragraph">
          <?php
          
           $fc = field_collection_field_get_entity($item);
           
$items = field_get_items('node', $node, 'field_title_description');

$n = 0;
$cnt = count($items);
if(is_array($items)){
foreach ($items as $item) {
 $fc = field_collection_field_get_entity($item);
 ?>
<div class="rowfc rowfc-<?php print $n%2==1 ? 'R' : 'L';if( strlen($fc->field_btitle['und'][0]['value']) < 10 ) print ' empty_ro';?>">
  <div class="btitle"><?php print $fc->field_btitle['und'][0]['value'];?></div>
  <div class="desc <?php if($cnt == ($n+1) || $cnt == ($n+2)  ) print " no_border";?>"><?php print isset($fc->field_bdescription['und'][0]['value']) ? $fc->field_bdescription['und'][0]['value'] : "";?></div>
  <?php if( isset($fc->field_pdf1['und'][0]['fid']) && $fc->field_pdf1['und'][0]['fid'] ){?>
  <?php foreach($fc->field_pdf1['und'] as $key => $pdf) { ?>
  
    <div class="pdf">
      
      <?php
      $uri = $pdf['uri'];
      $file_uri = file_create_url( $uri );
      ?>
      <a href="<?php print url($file_uri); ?>">
        <?php print $pdf['description'];?>
      </a>
      <a href="<?php print url($file_uri); ?>" class="bg"></a>     
      <div class="clearfix"></div>
	</div>
   
	<?php } ?>
<?php }?>
</div>  
<?php 

if($n%2==1) print'<div class="clearfix"></div>';
  $n++;
 
  }
}
          
//print render($content['field_title_description']);
/*$block = module_invoke('views', 'block_view', 'fieldcolection-block');
print render($block['content']);*/
?>
            <div class="clearfix"></div>
        </div>
        
    </div>
</div>
</div>