<?php 
global $language;
$lang = $language->language;
$node_ref = isset($node->field_banner_reference['und'][0]['node']) ? $node->field_banner_reference['und'][0]['node'] : [];
$node_files = isset($node->field_files['und']) ? $node->field_files['und'] : [];

$path = 'node/'.$node->nid;
$parent = menu_link_get_preferred($path);
$p=menu_link_load($parent['plid']);
$parameters = array(
    'active_trail' => array($parent['mlid']),
    'only_active_trail' => FALSE,
    'min_depth' => $parent['depth'],
    'max_depth' => $parent['depth'],
    'conditions' => array('plid' => $parent['plid']),
  );
$children = menu_build_tree($parent['menu_name'], $parameters);
foreach($children as $v)
{
  	$dropentries[] = l($v['link']['link_title'],$v['link']['link_path'] , array('query'=>array('o'=>1)));
}

switch($lang)
{
	case 'en':
		$locale = 'en_US';
		break;
	case 'fr':
		$locale = 'fr_FR';
		break;
	case 'es':
		$locale = 'es_ES';
		break;
	case 'pl':
		$locale = 'pl_PL';
		break;
	case 'ru':
		$locale = 'ru_RU';
		break;
	case 'zh-hans':
		$locale = 'en_US';
		break;
}
?>

	<div class="banner-locations">
		<div class="banner-locations-inner">
			<div class="locations-ptitle"><?php print t('News & Press')?></div>
		</div>
	</div>
	<div class="news_press_links">
	    <div class="wrap wrap_news fl"><?php print l( t('NEWS') , 'news');?></div>
	    <div class="wrap wrap_press fl"><?php print l( t('PRESS') , 'press');?></div>
	    <div class="clearfix"></div>
	</div>
	<?php 
		$nodeRefNid = isset($node_ref->nid) ? $node_ref->nid : 0;
		$nodeRefNidStr = " node_ref_nid_".$nodeRefNid; 
	?>
	<div id="node-<?php print $node->nid; ?>" class="<?php print $classes . $nodeRefNid;?>">
		<div class="node-inner">
			<div class="row news_single">
				<div class="col-md-12">
					<span class="boc_marche boc_<?php echo $node->field_marche['und'][0]['value']; ?>"></span>
				</div>
				<div class="col-md-4 visible-xs">
					<img class="img-responsive" src="<?php echo file_create_url($node->field_image['und'][0]['uri']); ?>" />
				</div>			
				<div class="col-md-8">
					<div class="title"><?php echo $node->title; ?></div>
					<div class="date"><?php echo date('d/m/Y', strtotime($node->field_date['und'][0]['value'])); ?></div>
					<div class="brief"><?php echo $node->field_brief['und'][0]['value']; ?></div>
					<div class="body"><?php print $node->body['und'][0]['value'];?></div>
					<div class="rs text-right">
						<table align="right" style="border-collapse:separate;border-spacing:10px;">
							<tr>
								<td class="td-facebook">
									<!-- Facebook -->
									<div id="fb-root"></div>
									<script>
									//document.getElementById("og-photo").content="<?php echo file_create_url($node->field_image['und'][0]['uri']); ?>";
									//document.getElementById("link-photo").href="<?php echo file_create_url($node->field_image['und'][0]['uri']); ?>";
									var url = "//connect.facebook.net/" + "<?php echo $locale; ?>" + "/sdk.js#xfbml=1&version=v2.7&appId=119245458102011";
									(function(d, s, id) {
									  var js, fjs = d.getElementsByTagName(s)[0];
									  if (d.getElementById(id)) return;
									  js = d.createElement(s); js.id = id;
									  js.src = url;
									  fjs.parentNode.insertBefore(js, fjs);
									}(document, 'script', 'facebook-jssdk'));</script>
									<div class="fb-share-button" data-href="<?php echo url($path, array('absolute' => TRUE)); ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.recette.boccard.com%2Ffr%2Factualites%2Fcontrat-nucleaire&amp;src=sdkpreparse"><?php print t('Share'); ?></a></div>
								</td>
								<td class="td-twitter">
									<!-- Twitter -->
									<a href="<?php echo url($path, array('absolute' => TRUE)); ?>" class="twitter-share-button" data-show-count="false">Tweet</a>
									<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
								</td>
								<td class="td-linkedin">
									<!-- LinkedIn -->
									<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: <?php echo $locale; ?></script>
									<script type="IN/Share"></script>
								</td>
							</tr>
						</table>
					</div>
					<div class="back">
						<?php 
							// Define link
							$history = $_SERVER["HTTP_REFERER"];
							if(strpos($history, 'boccard.com') != -1 && strlen($history))
							{
								$url = 'javascript:history.back()';
							}
							else
							{
								$url = url('actualites-presse');
							}
						?>
						<a href="<?php echo $url; ?>">&lt;<?php print t('Back')?></a>
					</div>
				</div>
				<div class="col-md-4 hidden-sm">
					<img class="img-responsive" src="<?php echo file_create_url($node->field_image['und'][0]['uri']); ?>" />
				</div>
			</div>
		</div>
	</div>