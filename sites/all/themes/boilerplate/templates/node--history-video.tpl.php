<?php
global $language;
$lang = $language->language;
$node_ref = $node->field_banner_reference['und'][0]['node'];
$node_files = $node->field_files['und'];

$path = 'node/'.$node->nid;
$parent = menu_link_get_preferred($path);
$p=menu_link_load($parent['plid']);
$parameters = array(
    'active_trail' => array($parent['mlid']),
    'only_active_trail' => FALSE,
    'min_depth' => $parent['depth'],
    'max_depth' => $parent['depth'],
    'conditions' => array('plid' => $parent['plid']),
);

$children = menu_build_tree($parent['menu_name'], $parameters);
foreach($children as $v){
    $dropentries[] = l($v['link']['link_title'],$v['link']['link_path'] , array('query'=>array('o'=>1)));
}
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; print " node_ref_nid_".$node_ref->nid;?>">
    <div id="history" class="node-inner">

        <div class="banner relative">
            <img alt="" src="<?php print base_path().  path_to_theme();?>/images/banner-history.jpg" />
            <div class="absolute absolute-dropdown_title_brief">
                <h2 class="title_sml"><?php print t('Boccard\'s DNA')?></h2>
                <h1 class="title"><?php print $node->title;?></h1>
                <div class="brief"><?php print t('A century of expertise')?></div>
            </div>
        </div>
        <div class="video-wrapper">
            <div class="video-container">
                <iframe src="<?php print $node->field_youtube['und'][0]['value'];?>" width="800" height="450" frameborder="0"></iframe>
            </div>
        </div>
        <h2><?php print $content['body']['#title'];?></h2>
        <?php print $node->body['und'][0]['value'];?>
        <?php if($node->field_contenu_visible['und'][0]['value'] == 1): ?>
            <h2><?php print $content['field_contenu']['#title'];?></h2>
            <?php print $node->field_contenu['und'][0]['value'];?>
        <?php endif; ?>
    </div>
</div>