<div class="partners_tpl_page">
    
<div class="banner-locations">
<div class="banner-locations-inner">
    <h1 class="locations-ptitle"><?php print t('Membership & Partnership')?></h1>
</div>
</div>

<div class="padding_2">
<?php 
$cnt=count($rows);
foreach ($rows as $row_count => $row):
foreach ($row as $field => $content): 
  switch($field){
    case 'body' : $body = $content; break;
    case 'nid' : $nid = $content; break;
    case 'title' : $title_f = $content; break;
    case 'field_image' : $field_image = $content; break;
  }
?>
<?php endforeach; ?>
<div class="bpa<?php if($row_count==0) print ' first';if($row_count==($cnt-1)) print ' last';?>">
    <div class="image fl"><?php print $field_image?></div>
    <div class="fr">
        <div class="title"><?php print $title_f?></div>
        <div class="body"><?php print $body?></div>
    </div>
    <div class="clearfix"></div>
</div>        
    
<?php endforeach; ?>
</div>
</div>
