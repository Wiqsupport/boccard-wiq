<?php
global $language;
$lang = $language->language;
$node_ref = $node->field_banner_reference['und'][0]['node'];
$node_files = $node->field_files['und'];

$path = 'node/'.$node->nid;
$parent = menu_link_get_preferred($path);
$p=menu_link_load($parent['plid']);
$parameters = array(
    'active_trail' => array($parent['mlid']),
    'only_active_trail' => FALSE,
    'min_depth' => $parent['depth'],
    'max_depth' => $parent['depth'],
    'conditions' => array('plid' => $parent['plid']),
);
$values = field_get_items('node', $node, 'field_values');

$children = menu_build_tree($parent['menu_name'], $parameters);
foreach($children as $v){
    $dropentries[] = l($v['link']['link_title'],$v['link']['link_path'] , array('query'=>array('o'=>1)));
}
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; print " node_ref_nid_".$node_ref->nid;?>">
    <div class="node-inner">
        <?php $banner_url = file_create_url($node->field_header_img['und'][0]['uri']); ?>
        <div class="banner" style="background-image: url(<?php echo $banner_url; ?>)">
            <div class="my_node_subtitle"><?php print $node->field_subtitle['und'][0]['value'];?></div>
            <h1 class="my_node_title"><?php print $node->title;?></h1>
        </div>
        <div class="row vision">
            <div class="col-sm-8">
                <div class="vision_header"><?php echo t('Our vision'); ?></div>
                <h2 class="quote"><?php print $node->field_vision_title['und'][0]['value'];?></h2>
                <div class="visible-xs-block img-wrapper">
                    <?php print render($content['field_vision_img']);?>
                    <div class="caption"><span><?php print $content['field_vision_img'][0]['#item']['title']; ?></span></div>
                </div>
                <div class="field_vision_text">
                    <div class="vision_header"><?php echo t('Our mission'); ?></div>
                    <div class="quote"><?php print $node->body['und'][0]['value'];?></div>
                </div>
            </div>
            <div class="col-sm-4 img-wrapper hidden-xs">
                <?php print render($content['field_vision_img']);?>
                <div class="caption right"><span><?php print $content['field_vision_img'][0]['#item']['title']; ?></span></div>
            </div>
        </div>
        <div class="row"><div class="col-sm-12 values_header"><?php echo t('Our values'); ?></div></div>
        <div class="row values-navigation hidden-xs">
            <ul>
                <?php
                $i = 0;
                foreach($values as $value) {
                    $item = field_collection_field_get_entity($value);
                    print '<li><a href="#value-'. $i .'" class="smoothScroll">' . $item->field_value_title['und'][0]['value'] . '</a></li>';
                    $i++;
                }
                ?>
            </ul>
        </div>
        <?php $i = 0; ?>
        <?php foreach($values as $value): ?>
            <?php $col_left_class = $i % 2 != 0 ? "col-sm-push-6" : ""; ?>
            <?php $col_right_class = $i % 2 != 0 ? "col-sm-pull-6" : ""; ?>
            <?php $item = field_collection_field_get_entity($value); ?>
            <?php $style_array = array('path' => $item->field_value_image['und'][0]['uri'], 'style_name' => 'large');?>
            <div class="row values" id="value-<?php echo $i; ?>">
                <div class="col-sm-6 <?php echo $col_left_class; ?> hidden-xs">
                    <?php print theme('image_style', $style_array); ?>
                    <div class="caption"><span><?php print $item->field_value_image['und'][0]['title']; ?></span></div>
                </div>
                <div class="col-sm-6 <?php echo $col_right_class; ?>">
                    <div class="value_header">
                        <h2><?php print $item->field_value_title['und'][0]['value']; ?></h2>
                        <div class="quote"><?php print $item->field_value_quote['und'][0]['value']; ?></div>
                    </div>
                    <div class="visible-xs-block img-wrapper">
                        <?php print theme('image_style', $style_array); ?>
                        <div class="caption"><span><?php print $item->field_value_image['und'][0]['title']; ?></span></div>
                    </div>
                    <div class="value_text">
                        <?php $figure = $item->field_value_figure['und'][0]['value']; ?>
                        <?php $subtitle = $item->field_value_subtitle['und'][0]['value']; ?>
                        <?php if ($subtitle): ?>
                            <h3><?php print $subtitle; ?></h3>
                        <?php endif; ?>
                        <?php if ($figure): ?>
                            <div class="value_figure"><?php print floor($item->field_value_figure['und'][0]['value']); ?></div>
                        <?php endif; ?>
                        <?php print $item->field_value_text['und'][0]['value']; ?>
                        <div class="value_note"><?php print $item->field_value_note['und'][0]['value']; ?></div>
                    </div>
                </div>
            </div>
            <?php $i++; ?>
        <?php endforeach; ?>
    </div>


</div>