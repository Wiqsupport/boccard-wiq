<?php
$lang=$language->language;
$langconcat='';
if($lang<>'en') $langconcat = "_".$lang;
if($lang=='zh-hans') $langconcat = "_cn";
?>
<div id="main">
    <div class="overlay-grey"></div>
    <div class="popup" id="popup_video_home">
        <div class="popup_inner relative">
            <div class="close_popup"></div>
            <div id="video_iframe">
                <?php if($lang=='zh-hans') { ?>
                    <iframe width="980" height="600" src="http://player.youku.com/embed/<?php print variable_get('home_page_video'.$langconcat);?>" frameborder="0" allowfullscreen></iframe>
                <?php } else { ?>
                    <div class="youtube_player" videoID="<?php print variable_get('home_page_video'.$langconcat);?>" width="980" height="600"></div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="container relative">


        <div class="discover_boccard_new_btn absolute hidden-xs">
            <div class="bg fl"></div>
            <div class="txt fr"><?php print t('Step into "Our First 100 Years" <br>history: 1918-2018'); ?></div>
            <div class="clearfix"></div>
        </div>


        <div>
            <?php print render($page['header'])?>
        </div>
        <div id="content-front">
            <div class="inner-padding">




                <?php print $messages;?>
                <?php print render($tabs);?>
                <?php print render($page['content_front'])?>


                <div id="home_contactBtn" class="contact_have_proj">
                    <div class="txt_big"><?php print t("Contact Us !");?></div>
                    <a class="main_btn_cont text-uppercase" href="<?php print url('contacts')?>"><?php print t("Contact FORM");?></a>
                </div>


            </div>
        </div>

        <footer>
            <?php print render($page['footer'])?>
        </footer>

    </div>
</div>