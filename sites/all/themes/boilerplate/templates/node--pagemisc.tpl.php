<div class="banner-locations">
    <div class="banner-locations-inner cookies">
        <h1 class="locations-ptitle"><?php print $node->title;?></h1>
    </div>
</div>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes;?>">
    <div class="node-inner">
        <div class="padd-class">
            <?php
                hide($content['links']);
                //print render($content);
                print $node->body['und'][0]['value'];
            ?>
        </div>
    </div>
</div>