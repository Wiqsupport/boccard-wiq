<div id="main">
<div class="container">
      <div>
        <?php print render($page['header'])?>
      </div>
      
    <div id="content-pages">
        <div class="inner-padding">
          <?php print $messages;?>  
            
<div class="banner-locations">
<div class="banner-locations-inner search">
    <h1 class="locations-ptitle"><?php print t('Results for : ').urldecode(arg(2));?></h1>
</div>
</div>
            
            
            
   
            <div class="search_wrapper">
              <?php print render($page['content']);?>
            </div>          
        </div>
    </div>


    
    <footer>
      <?php print render($page['footer'])?>
    </footer>


  </div>
</div>