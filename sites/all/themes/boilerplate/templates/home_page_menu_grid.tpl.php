<?php
global $language;
$lang = $language->language;
switch($lang){
  case 'fr' : 
		$mnu_name = "menu-main-menu-fr";
		$plant_process_main = 799;//  <----------------- fix this
		$and_l = '&';
		break;
  case 'es' : 
		$mnu_name = "menu-main-menu-es";
		$plant_process_main = 888;
		$and_l = 'Y';
		break;
  case 'zh-hans' : 
		$mnu_name = "menu-main-menu-cn";
		$plant_process_main = 1020;
		$and_l = '&';
		break;
	case 'pl' : 
		$mnu_name = "menu-main-menu-pl";
		$plant_process_main = 1193;
		$and_l = '&';
		break;
	case 'ru' : 
		$mnu_name = "menu-main-menu-ru";
		$plant_process_main = 1363;
		$and_l = '&';
		break;
	default :
		$and_l = '&';
		$mnu_name = "main-menu";
		$plant_process_main = 976;
		break;
}
$mtree = menu_tree($mnu_name);
$mtitle= isset($mtree[$plant_process_main]['#below'][$mid]['#title']) ? $mtree[$plant_process_main]['#below'][$mid]['#title'] : "";
$subs=array();
$paretnClass = "";
?>  
<div id="wrapper-<?php print $style;?>" class="relative">
<div class="absolute white-layer-trick white-layer-trick-<?php print $style;?>"></div>
<div class="custom_module_tpl_blocks set-<?php print $set;?>">
    <h2 class="btitle relative l2"><?php print $mtitle?>
       <?php if($set==1){?> <div class="absolute and_abs hidden-xs hidden-md hidden-sm"><?php print $and_l?></div><?php }?>
    </h2>
<ul class="wrapper_blocks" rel="<?php print $style;?>" set="set-<?php print $set;?>">
<?php
$cntP=0;
if(isset($mtree[$plant_process_main]['#below'][$mid]['#below']) && count($mtree[$plant_process_main]['#below'][$mid]['#below']) > 0){
foreach( $mtree[$plant_process_main]['#below'][$mid]['#below'] as $key=>$v ){
$cntP++;
 if ( isset($v['#title']) && $v['#title']<>'' ){
 
    $hasSubs='';
    if(sizeof($v['#below']) > 0 ) {
      $subs[$key] = $v['#below']; $hasSubs=" hasSubs";
    }
    else $hasSubs=" doesNOThaveSub";
    ?>
    <li myrow="<?php print $cntP?>" class="triger_drop_cat mid-<?php print $key.' '.strtolower(drupal_clean_css_identifier( $v['#title'] )); print $hasSubs;?>" rel="blk-<?php print $key ;?>">
       <?php if($hasSubs<>' doesNOThaveSub'){?>
     <a class="bg"></a>
      <div class="title"><?php print $v['#title'] ;?></div>
     <?php }else{ ?>
      <a class="bg" href="<?php print url($v['#href'])?>"></a>
      <?php print l($v['#title'] , $v['#href'] , array('attributes'=>array('id'=>'doesNOThaveSubID' , 'class'=>array('title') )));?>
     <?php }?>
     
 </li>
<?php 
if ( ($cntP%2) ==0 ){

 ?>
 <li class="split_li split_li-<?php print $cntP;?> set-<?php print $set;?> col-xs-12"><div class="plants_detail_box <?php print ' cntp-'.$cntP;?> set-<?php print $set;?>">
         <div class="det_top"></div>
         <div class="det_middle">
        <?php 
        foreach( $subs as $k=>$vv ){
          $below=ca($vv);
          $cntBLW=count($below);
          ?>
             <div class="blk blk-<?php print $k;?>">
        <?php $n=0;  foreach( $below as $c=>$b ){?>
<?php if($cntBLW>1 && sizeof($b['#below'])>0){?>            
                 <div class="<?php print (($n%2)==1) ? 'right-j fr' : 'left-j fl';?>">
<?php }?>
            <?php if(sizeof($b['#below'])>0) $paretnClass='hasChildren fl';?>
                     <div class="title2 <?php print $paretnClass.' c-'.$c; $paretnClass='';?>">
                    <?php print $b['#href'] <> '<nolink>' ? l($b['#title'],$b['#href'] ,array('attributes'=> array('nid'=>$k))) : $b['#title'] ;?>
                     </div>
                    
             <?php if(sizeof($b['#below'])>0){?>
                     <div class="fl childs">
                      <?php 
                      $ar=ca($b['#below']);
                      foreach($ar as $kkk=>$vvv){?>
                         <div class="smallest"><?php print l($vvv['#title'],$vvv['#href'] ,array('attributes'=> array('nid'=>$k)))?></div>
                      <?php }?>
                     </div>
                <?php }?>
                     
<?php if($cntBLW>1 && sizeof($b['#below'])>0){?>            
                 </div>
<?php }?>
  
        <?php 
                      $n++;
                        
                      }?>
                        
                        
                 <div class="clearfix"></div>
             </div>
        <?php }$subs=array();?>
         </div>
         <div class="det_bottom"></div>
     </div>
 </li>
 
 
 <?php } }
}}?>
</ul>

<div class="clearfix"></div> 
</div>
</div>
