<div class="row block-views-news-block">
    
  <?php foreach ($rows as $row_count => $row):
foreach ($row as $field => $content): 
  switch($field){
    case 'field_image' :
      $field_image = $content;
    break;
    case 'field_brief' :
      $field_brief = $content;
    break;
    case 'title' :
      $field_title = $content;
    break;
    case 'nid' :
      $nid = $content;
    break;
  }
?>
<?php endforeach; ?>
    
 <div class="col-xs-4 row-wrap-news row-wrap-news-<?php print $row_count;?> <?php print $row_count==0 ? 'first' : ''; ?>">
     <div class="image relative">
         <div class="absolute maskit">
             <div class="title text-uppercase">
                 <div class="tn_title_p">
                     <a href="<?php print url('news').'#'.strtolower(drupal_clean_css_identifier($field_title))?>">
                        <?php print $field_title;?>
                     </a>
                 </div>
             </div>
             <div class="show-hide-hover">
                <div class="show-hide-hover_inner">
                   <div class="field_brief"><?php print $field_brief;?></div>
                   <div class="more">
                       <div class="row-white-right fl"></div>
                       <a href="<?php print url('news').'#'.strtolower(drupal_clean_css_identifier($field_title));?>" class="text-uppercase more fl"><?php print t('read more');?></a>
                       <div class="clearfix"></div>
                   </div>
                </div>
             </div>
         </div>
         
         <a href="<?php print url('news').'#'.strtolower(drupal_clean_css_identifier($field_title))?>"><?php print $field_image;?></a>
         
     </div>
 </div>



<?php endforeach; ?>

    <div class="clearfix"></div>
    
    

    <div class="more-btn text-uppercase"><a href="<?php print url('news')?>"><?php print t("See more news")?></a></div>
</div>
