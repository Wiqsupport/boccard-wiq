<div class="testi-hr-block">
    <div class="wrapper">
        <div class="jcarousel-wrapper">
<div class="jcarousel-HRpolicy">
<ul>
<?php
$cnt = count($rows);
$str='';
$video_array=array();
$video_youku_array=array();
foreach ($rows as $row_count => $row):
  foreach ($row as $field => $content):
  switch($field){
    case 'title' : $field_title = $content;break;
    case 'field_position' : $field_position = $content;break;
    case 'field_region' : $field_region = $content;break;
    case 'field_youtube_link' : $field_youtube_link = $content;break;
    case 'field_youku_link' : $field_youku_link = $content;break;
    case 'nid' : $nid = $content;break;

  }
?>
<?php endforeach; 
$video_array[]=$field_youtube_link;
$video_youku_array[] = $field_youku_link;
$str.='
<li rel="'.$row_count.'" class="'.($row_count==0 ? 'active':'').'">
<div class="wrapper--">
<div class="cell--">
<div class="testi-hr-blk-row">
<div class="title">'.$field_title.'</div>
<div class="field_position">'.$field_position.'</div>
<div class="field_region">'.$field_region.'</div>
</div>
</div>
</div>
</li>';
endforeach; 
print $str;
?>
</ul>
</div>
             <?php if(sizeof($video_array) >4){?><?php }?>
              <a href="#" class="jcarousel-control-prev"></a>
              <a href="#" class="jcarousel-control-next"></a>
            
            
        </div>
    </div>
</div>


<div class="videos-testimonials">
    
    <?php foreach($video_array as $kv=>$vid){?>
	  <div class="video-testimonial video-testimonial-<?php print $kv?>">
		  <iframe class="yt_player_iframe" width="980" height="600" src="https://www.youtube.com/embed/<?php print $vid;?>?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
	  </div>
    <?php }?>
	
	<?php foreach($video_youku_array as $kv=>$vid){?>
		<?php if($vid<>''){?>
	  	<div class="video-youku-testimonial video-testimonial video-testimonial-<?php print $kv?>">
			
			<iframe class="yt_player_iframe" width="980" height="600" src="http://player.youku.com/embed/<?php print $vid;?>" frameborder="0" allowfullscreen></iframe>
		
		</div>
		<?php } ?>
    <?php }?>
</div>