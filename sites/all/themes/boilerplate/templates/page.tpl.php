<?php 
$lang=$language->language;
$langconcat='';
if($lang<>'en') $langconcat = "_".$lang;
if($lang=='zh-hans') $langconcat = "_cn";
?>
<div id="main">
<div class="overlay-grey"></div>

  <div class="container">
      <div>
        <?php print render($page['header'])?>
      </div>
      
    <div id="content-pages">
        <div class="inner-padding">
            

<?php if(arg(0)=='sitemap') {//sitemap?>
<div class="banner-locations">
  <div class="banner-locations-inner sitemap">
      <div class="locations-ptitle"><?php print t('Site map');?></div>
  </div>
</div>
<?php }?> 
            
<div class="<?php if(arg(0)=='sitemap') print 'padd-class padd-class-sitemap';?>">
<?php print $messages;?>            
<?php print render($tabs);?>            
<?php print render($page['content'])?> 
</div>    
            
            
        </div>
    </div>
      
      
      

    <?php if(arg(0)=='news' || arg(0)=='press' || arg(0) == 'actualites-presse'){?>
      <div id="block-custom-front_newsletter_form">
      <div class="block-inner">
      <?php $block = module_invoke('custom', 'block_view', 'front_newsletter_form');
      print render($block['content']);?>
      </div>
      </div>
    <?php }?>
    <footer>
      <?php print render($page['footer'])?>
    </footer>


  </div>
</div>