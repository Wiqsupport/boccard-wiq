<div class="swiper-container">
    <div class="swiper-wrapper">
        <?php foreach ($rows as $row_count => $row):
            foreach ($row as $field => $content):
                switch($field){
                    case 'field_image_mobile' : $field_image_mobile = $content;break;
                    case 'field_image' : $field_image = $content;break;
                    case 'field_brief' : $field_brief = $content;break;
                    case 'field_title_class' : $field_title_class = $content;break;
                    case 'title' : $field_title = $content;break;
                    case 'nid' : $nid = $content;break;
                    case 'field_url' : $field_url = $content;break;
                }
                ?>
            <?php endforeach; ?>

            <div class="swiper-slide relative">
                <?php if ($field_url): ?>
                    <a href="<?php echo $field_url; ?>">
                <?php endif; ?>
                <div class="swiper-slider-inner">
                    <div class="absolute text_on_banner text_on_banner-<?php print $nid?>">
                        <div class="big <?php print $field_title_class;?>"><?php print $field_title;?></div>
                        <div class="sml"><?php print $field_brief;?></div>
                     </div>
                    <div class="hb_field_image_mobile">
                        <?php print $field_image_mobile;?>
                    </div>
                    <div class="hb_field_image">
                        <?php print $field_image;?>
                    </div>
                </div>
                <?php if ($field_url): ?>
                    </a>
                <?php endif; ?>
            </div>

        <?php endforeach; ?>
    </div>
    <!--<div class="swiper-pagination"></div>-->

    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
</div>
