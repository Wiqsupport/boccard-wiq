<div class="hr-block">
<?php $cnt = count($rows);foreach ($rows as $row_count => $row):
global $language;
$lang = $language->language;
  foreach ($row as $field => $content): 
  switch($field){
    case 'title' : $field_title = $content;break;
    case 'body' : $body = $content;break;
    case 'field_image' : $field_image = $content;break;
    case 'nid' : $nid = $content;break;
  }
?>
<?php endforeach; ?>
    <div nid="<?php print $nid;?>" class="hr-blk-row hr-blk-row-<?php print($row_count == 0) ? '' : ''; if($cnt==$row_count+1)print ' last' ?>">
     <div class="<?php if($field_image) print'col-lg-8'; else print 'col-lg-12';?>">
        <div class="title"><?php print $field_title;?></div>
        <div class="body"><div class="binner"><?php print $body;?></div></div>
     </div>
     <?php if($field_image){?>
        <div class="col-lg-4">
        <div class="fr">
            <div class="img100p"><?php print $field_image;?></div>
                <?php 
                if( in_array($nid, array(200,1497,1577,1995)) ){?>
                <div class="hr_page join_us ancien_club">
                    <a href="mailto:forlife@boccard.com"><?php print t('Join the club');?></a>
                </div>
                <?php }else{?>
                <div class="hr_page join_us">
                    <a href="<?php print url('recruitment_site');?>"><?php print t('Join Us');?></a>
                </div>
                <?php }?>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
     <?php }?>
  </div>
<?php endforeach; ?>
    <div class="clearfix"></div>
</div>
