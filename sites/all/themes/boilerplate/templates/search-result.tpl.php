<?php
/**
 * @file
 * Default theme implementation for displaying a single search result.
 *
 * This template renders a single search result and is collected into
 * search-results.tpl.php. This and the parent template are
 * dependent to one another sharing the markup for definition lists.
 *
 * Available variables:
 * - $url: URL of the result.
 * - $title: Title of the result.
 * - $snippet: A small preview of the result. Does not apply to user searches.
 * - $info: String of all the meta information ready for print. Does not apply
 *   to user searches.
 * - $info_split: Contains same data as $info, split into a keyed array.
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Default keys within $info_split:
 * - $info_split['module']: The module that implemented the search query.
 * - $info_split['user']: Author of the node linked to users profile. Depends
 *   on permission.
 * - $info_split['date']: Last update of the node. Short formatted.
 * - $info_split['comment']: Number of comments output as "% comments", %
 *   being the count. Depends on comment.module.
 *
 * Other variables:
 * - $classes_array: Array of HTML class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $title_attributes_array: Array of HTML attributes for the title. It is
 *   flattened into a string within the variable $title_attributes.
 * - $content_attributes_array: Array of HTML attributes for the content. It is
 *   flattened into a string within the variable $content_attributes.
 *
 * Since $info_split is keyed, a direct print of the item is possible.
 * This array does not apply to user searches so it is recommended to check
 * for its existence before printing. The default keys of 'type', 'user' and
 * 'date' always exist for node searches. Modules may provide other data.
 * @code
 *   <?php if (isset($info_split['comment'])): ?>
 *     <span class="info-comment">
 *       <?php print $info_split['comment']; ?>
 *     </span>
 *   <?php endif; ?>
 * @endcode
 *
 * To check for all available data within $info_split, use the code below.
 * @code
 *   <?php print '<pre>'. check_plain(print_r($info_split, 1)) .'</pre>'; ?>
 * @endcode
 *
 * @see template_preprocess()
 * @see template_preprocess_search_result()
 * @see template_process()
 *
 * @ingroup themeable
 */
//d($result);

if( isset($result['node']->bundle) ){
  $bundle = $result['node']->bundle;
  $entity_id= $result['node']->entity_id;
}


$remo = array("EN" , "ES","FR" , "CN");
$my_sentence  = str_replace($remo , "", $result['snippets']['content'][0] . $result['snippets']['content'][1]);

?>
<?php if($bundle=='interesting_facts'){ ?>
<li class="<?php print $classes.' bundle-'.$bundle.' entity_id-'.$entity_id ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <h3 class="title"<?php print $title_attributes; ?>>
      <a href="<?php print url('boccard-glance');?>"><?php print $title; ?></a>
  </h3>
  <?php print render($title_suffix); ?>
  <div class="search-snippet-info">
  <?php if ($snippet): ?>
  <p class="search-snippet"<?php print $content_attributes; ?>>
  <?php print $my_sentence;?>
  </p>
  <div class="srch_lnk">
      <a href="<?php print url('boccard-glance');?>">
        <?php print $url;//$result['link']?></a>
  </div>
  <?php endif; ?>
  </div>
  </li>
<?php }elseif($bundle=='press'){ ?>
<li class="<?php print $classes.' bundle-'.$bundle; ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <h3 class="title"<?php print $title_attributes; ?>>
      <a href="<?php print url('press/'.$entity_id);?>"><?php print $title; ?></a>
  </h3>
  <?php print render($title_suffix); ?>
  <div class="search-snippet-info">
  <?php if ($snippet): ?>
  <p class="search-snippet"<?php print $content_attributes; ?>>
  <?php 
  print $my_sentence;?>
  </p>
  <div class="srch_lnk">
      <a href="<?php print url('press/'.$entity_id);?>">
        <?php print $url;//$result['link']?></a>
  </div>
  <?php endif; ?>
  </div>
  </li>
<?php }elseif($bundle=='history'){ ?>
<li class="<?php print $classes.' bundle-'.$bundle; ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <h3 class="title"<?php print $title_attributes; ?>>
      <a href="<?php print url('history');?>"><?php print $title; ?></a>
  </h3>
  <?php print render($title_suffix); ?>
  <div class="search-snippet-info">
  <?php if ($snippet): ?>
  <p class="search-snippet"<?php print $content_attributes; ?>>
  <?php print $my_sentence;?>
  </p>
  <div class="srch_lnk">
      <a href="<?php print url('history');?>">
        <?php print $url;//$result['link']?></a>
  </div>
  <?php endif; ?>
  </div>
  </li>
<?php }elseif($bundle=='news'){?>
<li class="<?php print $classes.' bundle-'.$bundle; ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <h3 class="title"<?php print $title_attributes; ?>>
      <a href="<?php print url('news/'.$entity_id);?>"><?php print $title; ?></a>
  </h3>
  <?php print render($title_suffix); ?>
  <div class="search-snippet-info">
  <?php if ($snippet): ?>
  <p class="search-snippet"<?php print $content_attributes; ?>>
  <?php print $my_sentence;?>
  </p>
  <div class="srch_lnk">
      <a href="<?php print url('news/'.$entity_id);?>">
        <?php print $url;//$result['link']?></a>
  </div>
  <?php endif; ?>
  </div>
  </li>
<?php } elseif( $bundle=='success_stories' ){
$node_ss = node_load($entity_id);

$field_comp_logo1 = $node_ss->field_comp_logo1['und'][0]['nid'];
$ref_nids = $node_ss->field_page_reference['und'];
$seen= array();
foreach($ref_nids as $nid_ref){
 if ( custom_node_exist( $nid_ref['nid']) && !in_array($node_ss->nid,$seen)){
   $seen[]=$node_ss->nid;
$node_ref_pg = node_load( $nid_ref['nid'] );
?>
    <li class="<?php print $classes.' bundle-'.$node_ref_pg->type.' nid-'.$node_ref_pg->nid.' entity_id-'.$entity_id; ?>"<?php print $attributes; ?>>
    <?php print render($title_prefix); ?>
    <h3 class="title"<?php print $title_attributes; ?>>
        <a href="<?php print url('node/'.$node_ref_pg->nid.'/'.$field_comp_logo1);?>"><?php print $node_ref_pg->title; ?></a>
    </h3>
    <?php print render($title_suffix); ?>
    <div class="search-snippet-info">
    <?php if ($snippet): ?>
    <p class="search-snippet"<?php print $content_attributes; ?>>
    <?php 
    $conti = custom_getWords( strip_tags($node_ss->body['und'][0]['value']) , 20 );
    $conti_srch = explode(' ' , arg(2));
    print custom_highlight_words($conti ,$conti_srch );?>
    </p>
    <div class="srch_lnk">
        <a href="<?php print url('node/'.$node_ref_pg->nid.'/'.$field_comp_logo1);?>">
          <?php print $url?>
        </a>
    </div>
    <?php endif; ?>
    </div>
    </li>
<?php } } } else{?>
<li class="<?php print $classes.' bundle-'.$bundle; ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <h3 class="title"<?php print $title_attributes; ?>>
      <a href="<?php print $url; ?>"><?php print $title; ?></a>
  </h3>
  <?php print render($title_suffix); ?>
  <div class="search-snippet-info">
  <?php if ($snippet): ?>
  <p class="search-snippet"<?php print $content_attributes; ?>>
  <?php print $my_sentence;?>
  </p>
  <div class="srch_lnk">
      
      <a href="<?php print $url;//$result['link']?>">
        <?php print $url;//$result['link']?></a>
  </div>
  <?php endif; ?>
  </div>
  </li>
  <?php } ?>