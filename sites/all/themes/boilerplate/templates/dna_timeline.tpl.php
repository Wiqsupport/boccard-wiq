<div id="history">
  
    <div class="banner relative">
        <img alt="" src="<?php print base_path().  path_to_theme();?>/images/banner-history.jpg" />
        <div class="absolute absolute-dropdown_title_brief">
            <h2 class="title_sml">Boccard's DNA</h2>
            <h1 class="title">History</h1>
            <div class="brief">A century of expertise</div>
        </div>
    </div>
      
</div>