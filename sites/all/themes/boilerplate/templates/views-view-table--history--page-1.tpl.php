<style type="text/css">
<?php $widthArray = array(242,540,438,264,420,300,468,390,438,630,305);?>
<?php for($i=0;$i<=10;$i++){?>
	.slide-<?php echo $i ?> .bg-slide{
		background: url('/sites/all/themes/boilerplate/images/slides/slide-<?php echo $i ?>.png') no-repeat center;
		width:<?php echo $widthArray[$i] ?>px;
	}
	.slide-<?php echo $i ?>{
		width:<?php echo $widthArray[$i] ?>px;
	}
<?php }?>
<?php $widthArray = array(0,255,423,294,474,516,444,498,390);?>
<?php for($i=1;$i<=8;$i++){?>
	.slide-repeat-<?php echo $i ?> .bg-slide{
		background: url('/sites/all/themes/boilerplate/images/slides/slide-repeat-<?php echo $i ?>.png') center;
		width:<?php echo $widthArray[$i] ?>px;
	}
	.slide-repeat-<?php echo $i ?>{
		width:<?php echo $widthArray[$i] ?>px;
	}
<?php }?>
</style>

<div id="history">
    <div class="banner relative">
        <img alt="" src="<?php print base_path().  path_to_theme();?>/images/banner-history.jpg" />
        <div class="absolute absolute-dropdown_title_brief">
            <h2 class="title_sml"><?php print t("Boccard's DNA");?></h2>
            <h1 class="title"><?php print t("History");?></h1>
            <div class="brief"><?php print t("A century of expertise");?></div>
        </div>
    </div>
<?php 
$v=1;
$a=0;
$dateArray = array();
foreach ($rows as $row_count => $row):
	foreach ($row as $field => $content): 
	  
	  switch($field){
		case 'title' : $field_title = $content;break;
		case 'field_focus' : $field_focus = $content;break;
		case 'body' : $description = $content;break;
		case 'field_class' : $field_class = $content;break;
		case 'field_hist_logo' : $field_hist_logo = $content;break;
	  }
	
	endforeach; 
 	$logos = explode('|' , $field_hist_logo);

 	$lisArray[$v]=array(
    	'date'=>$field_title,
    	'focus'=>$field_focus,
    	'description'=>$description,
    	'class'=>$field_class,
    	'logos'=>$logos
 	);
 	$dateArray[$a]=$field_title;
 	$v++;
	$a++;
endforeach; 

function check_value($array, $key, $val) {
    foreach ($array as $k=>$item)
        if (isset($item[$key]) && $item[$key] == $val)
            return $k;
    return false;
}

function displayBoxData($array,$index){
	$class = ($array[$index]['class']=='Yes')?"extra":"";
	return '<h3 class="'.$class.'">'.$array[$index]['date'].'</h3>'.$array[$index]['description'];
}

function displayImagebyIndex($array,$index,$class=""){
	$imageArray = $array[$index]['logos'];
	$display = '';
	if( $imageArray[0] ){
		$display .= '<div class="box-logos '.$class.'">
				<ul>';
		foreach($imageArray as $image){
			$display .= '<li>'.$image.'</li>';
        }
        $display .= '</ul>
            </div>';
     }
	 return $display;
}

$stanDate = range(1918,2015);

$array = array_diff(array_merge($stanDate,$dateArray),array_intersect($stanDate,$dateArray));

foreach($array as $value){
	$dateArray[$a]=$value;
	$a++;
}

asort($dateArray);

?>
 <div style="clear:both"></div>
 
 <div class="content">
     <div id="dock">
        <div class="dock-container" data-width="<?php echo (sizeof($dateArray)*14); ?>">
            <?php
            	$y=0;
				foreach($dateArray as $i){
					$returnExist = check_value($lisArray, 'date', $i);
					$myClass="";
					$myData="";
					$myBox="";
					$myIndex="";
					$arrowBox="";
					if( $returnExist != false ){
						$myIndex = $y;
						//$myBox = '<div class="imageSmall"><div>'.$i.'</div></div>';
						$arrowBox = '<div class="arrow-slider"></div>';
						$myBox = '<span>'.$i.'</span><img src="'.base_path().  path_to_theme().'/images/button.png" />';
						$myClass = ($lisArray[$returnExist]['class']=='Yes')?"extra":"";
						$myData = ($lisArray[$returnExist]['class']=='Yes')?"<div class=\"detailBox adjust-".$i."\"><h3>".$i."</h3>".$lisArray[$returnExist]['focus']."</div>":"<div class=\"detailBox hidden-lg  hidden-md hidden-sm\"><h3>".$i."</h3></div>";
						//$myData0 = ($lisArray[$returnExist]['class']=='Yes')?"<div class=\"detailBox hidden-lg\"><h3>".$i."</h3>".$lisArray[$returnExist]['focus']."</div>":"";
						$y++;
					}
			?>
            <a class="dock-item <?php echo $myClass ?>" href="javascript:;" data-index="<?php echo $myIndex; ?>">
            	<?php echo $arrowBox; ?>
                <?php echo $myBox; ?>
                <?php echo $myData; ?>
            </a> 
            <?php
				}
			?>
        </div>
     </div>
 </div>
	
    <div style="clear:both"></div>
    
    <div class="content history-main" id="content">

        <div class="history-slider">
            
          <div class="single-slide slide-1">
          	<div class="movie-clip movie-clip-1"></div>
            <div class="movie-clip movie-clip-2"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data right"><div class="arrow"></div><?php echo displayBoxData($lisArray,1); ?></div>
                <?php echo displayImagebyIndex($lisArray,1);?>
            </div>
          </div>
          
          <div class="single-slide slide-2">
          	<div class="movie-clip movie-clip-1" data-length="225" data-move="height"></div>
            <div class="movie-clip movie-clip-2" data-length="450" data-move="width"></div>
            <div class="movie-clip movie-clip-3" data-length="325" data-move="height"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data right"><div class="arrow"></div><?php echo displayBoxData($lisArray,2); ?></div>
                <?php echo displayImagebyIndex($lisArray,2);?>
            </div>
          </div>
          
          <div class="single-slide slide-3">
          	<div class="movie-clip movie-clip-1" data-length="40" data-move="height"></div>
            <div class="movie-clip movie-clip-2" data-length="130" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data top"><div class="arrow"></div><?php echo displayBoxData($lisArray,3); ?></div>
                <?php echo displayImagebyIndex($lisArray,3);?>
            </div>
          </div>
          
          <div class="single-slide slide-4">
          	<div class="movie-clip movie-clip-1" data-length="150" data-move="width"></div>
            <div class="movie-clip movie-clip-2" data-length="320" data-move="height"></div>
            <div class="movie-clip movie-clip-3" data-length="220" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
           		<?php echo displayImagebyIndex($lisArray,4,'hidden-xs hidden-sm hidden-md');?>
            	<div class="box-data bottom"><div class="arrow"></div><?php echo displayBoxData($lisArray,4); ?></div>
              <?php echo displayImagebyIndex($lisArray,4,'hidden-lg');?>
            </div>
          </div>
          
          <div class="single-slide slide-5">
          	<div class="movie-clip movie-clip-1" data-length="220" data-move="width"></div>
            <div class="movie-clip movie-clip-2" data-length="360" data-move="height"></div>
            <div class="movie-clip movie-clip-3" data-length="160" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data top"><div class="arrow"></div><?php echo displayBoxData($lisArray,5); ?></div>
                <?php echo displayImagebyIndex($lisArray,5);?>
            </div>
          </div>
          
          <div class="single-slide slide-6">
          	<div class="movie-clip movie-clip-1" data-length="145" data-move="width"></div>
            <div class="movie-clip movie-clip-2" data-length="320" data-move="height"></div>
            <div class="movie-clip movie-clip-3" data-length="480" data-move="width"></div>
            <div class="movie-clip movie-clip-4" data-length="220" data-move="height"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data right"><div class="arrow"></div><?php echo displayBoxData($lisArray,6); ?></div>
                <?php echo displayImagebyIndex($lisArray,6);?>
            </div>
          </div>
          
          <div class="single-slide slide-7">
          	<div class="movie-clip movie-clip-1" data-length="160" data-move="height"></div>
            <div class="movie-clip movie-clip-2" data-length="215" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data top"><div class="arrow"></div><?php echo displayBoxData($lisArray,7); ?></div>
                <?php echo displayImagebyIndex($lisArray,7);?>
            </div>
          </div>
          
          <div class="single-slide slide-8">
            <div class="movie-clip movie-clip-1" data-length="205" data-move="width"></div>
          	<div class="movie-clip movie-clip-2" data-length="180" data-move="height"></div>
            <div class="movie-clip movie-clip-3" data-length="115" data-move="width"></div>
          	<div class="movie-clip movie-clip-4" data-length="110" data-move="height"></div>
            <div class="movie-clip movie-clip-5" data-length="190" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<?php echo displayImagebyIndex($lisArray,8,'hidden-xs hidden-sm');?>
            	<div class="box-data bottom"><div class="arrow"></div><?php echo displayBoxData($lisArray,8); ?></div>
              <?php echo displayImagebyIndex($lisArray,8,'hidden-md hidden-lg');?>
            </div>
          </div>
          
          <div class="single-slide slide-9">
            <div class="movie-clip movie-clip-1" data-length="195" data-move="width"></div>
          	<div class="movie-clip movie-clip-2" data-length="105" data-move="height"></div>
            <div class="movie-clip movie-clip-3" data-length="120" data-move="width"></div>
          	<div class="movie-clip movie-clip-4" data-length="150" data-move="height"></div>
            <div class="movie-clip movie-clip-5" data-length="215" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data top"><div class="arrow"></div><?php echo displayBoxData($lisArray,9); ?></div>
                <?php echo displayImagebyIndex($lisArray,9);?>
            </div>
          </div>
          
          <div class="single-slide slide-10">
          	<div class="movie-clip movie-clip-1" data-length="240" data-move="width"></div>
            <div class="movie-clip movie-clip-2"data-length="80" data-move="height"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data left"><div class="arrow"></div><?php echo displayBoxData($lisArray,10); ?></div>
                <?php echo displayImagebyIndex($lisArray,10);?>
            </div>
          </div>

          <?php /*loop slides*/?>
          <?php for($m=11;$m<=sizeof($lisArray);$m=$m+8){?>
          <?php if($lisArray[$m]){?>
          <div class="single-slide slide-<?php echo $m; ?> slide-repeat-1">
          	<div class="movie-clip movie-clip-1" data-length="270" data-move="height"></div>
            <div class="movie-clip movie-clip-2" data-length="385" data-move="width"></div>
          	<div class="movie-clip movie-clip-3" data-length="270" data-move="height"></div>
            <div class="movie-clip movie-clip-4" data-length="115" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data top"><div class="arrow"></div><?php echo displayBoxData($lisArray,$m); ?></div>
              <?php echo displayImagebyIndex($lisArray,$m);?>
            </div>
          </div>
          <?php }?>
          
          <?php if(isset($lisArray[$m+1]) && $lisArray[$m+1]){?>
          <div class="single-slide slide-<?php echo $m+1; ?> slide-repeat-2">
            <div class="movie-clip movie-clip-1" data-length="130" data-move="width"></div>
          	<div class="movie-clip movie-clip-2" data-length="315" data-move="height"></div>
            <div class="movie-clip movie-clip-3" data-length="210" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<?php echo displayImagebyIndex($lisArray,$m+1,'hidden-xs hidden-sm');?>
            	<div class="box-data bottom"><div class="arrow"></div><?php echo displayBoxData($lisArray,$m+1); ?></div>
              <?php echo displayImagebyIndex($lisArray,$m+1,'hidden-md hidden-lg');?>
            </div>
          </div>
          <?php }?>
          
          <?php if(isset($lisArray[$m+2]) && $lisArray[$m+2]){?>
          <div class="single-slide slide-<?php echo $m+2; ?> slide-repeat-3">
            <div class="movie-clip movie-clip-1" data-length="220" data-move="width"></div>
          	<div class="movie-clip movie-clip-2" data-length="285" data-move="height"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data left"><div class="arrow"></div><?php echo displayBoxData($lisArray,$m+2);?></div>
                <?php echo displayImagebyIndex($lisArray,$m+2);?>
            </div>
          </div>
          <?php }?>
          
          <?php if(isset($lisArray[$m+3]) && $lisArray[$m+3]){?>
          <div class="single-slide slide-<?php echo $m+3; ?> slide-repeat-4">
          	<div class="movie-clip movie-clip-1" data-length="70" data-move="height"></div>
            <div class="movie-clip movie-clip-2" data-length="300" data-move="width"></div>
          	<div class="movie-clip movie-clip-3" data-length="315" data-move="height"></div>
            <div class="movie-clip movie-clip-4" data-length="235" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<?php echo displayImagebyIndex($lisArray,$m+3,'hidden-xs hidden-sm');?>
            	<div class="box-data bottom"><div class="arrow"></div><?php echo displayBoxData($lisArray,$m+3);?></div>
              <?php echo displayImagebyIndex($lisArray,$m+3,'hidden-md hidden-lg');?>
            </div>
          </div>
          <?php }?>
          
          <?php if(isset($lisArray[$m+4]) && $lisArray[$m+4]){?>
          <div class="single-slide slide-<?php echo $m+4; ?> slide-repeat-5">
            <div class="movie-clip movie-clip-1" data-length="240" data-move="width"></div>
          	<div class="movie-clip movie-clip-2" data-length="350" data-move="height"></div>
            <div class="movie-clip movie-clip-3" data-length="410" data-move="width"></div>
          	<div class="movie-clip movie-clip-4" data-length="70" data-move="height"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data right"><div class="arrow"></div><?php echo displayBoxData($lisArray,$m+4);?></div>
                <?php echo displayImagebyIndex($lisArray,$m+4);?>
            </div>
          </div>
          <?php }?>
          
          <?php if(isset($lisArray[$m+5]) && $lisArray[$m+5]){?>
          <div class="single-slide slide-<?php echo $m+5; ?> slide-repeat-6">
          	<div class="movie-clip movie-clip-1" data-length="85" data-move="height"></div>
            <div class="movie-clip movie-clip-2" data-length="115" data-move="width"></div>
          	<div class="movie-clip movie-clip-3" data-length="110" data-move="height"></div>
            <div class="movie-clip movie-clip-4" data-length="190" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<?php echo displayImagebyIndex($lisArray,$m+5,'hidden-xs hidden-sm');?>
            	<div class="box-data bottom"><div class="arrow"></div><?php echo displayBoxData($lisArray,$m+5);?></div>
              <?php echo displayImagebyIndex($lisArray,$m+5,'hidden-md hidden-lg');?>
            </div>
          </div>
          <?php }?>
          
          <?php if(isset($lisArray[$m+6]) && $lisArray[$m+6]){?>
          <div class="single-slide slide-<?php echo $m+6; ?> slide-repeat-7">
            <div class="movie-clip movie-clip-1" data-length="195" data-move="width"></div>
          	<div class="movie-clip movie-clip-2" data-length="105" data-move="height"></div>
            <div class="movie-clip movie-clip-3" data-length="120" data-move="width"></div>
          	<div class="movie-clip movie-clip-4" data-length="155" data-move="height"></div>
            <div class="movie-clip movie-clip-5" data-length="220" data-move="width"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data top"><div class="arrow"></div><?php echo displayBoxData($lisArray,$m+6);?></div>
                <?php echo displayImagebyIndex($lisArray,$m+6);?>
            </div>
          </div>
          <?php }?>
          
          <?php if(isset($lisArray[$m+7]) && $lisArray[$m+7]){?>
          <div class="single-slide slide-<?php echo $m+7; ?> slide-repeat-8">
          	<div class="movie-clip movie-clip-1" data-length="230" data-move="width"></div>
            <div class="movie-clip movie-clip-2" data-length="85" data-move="height"></div>
          	<div class="bg-slide"></div>
            <div class="box-container">
            	<div class="box-data left"><div class="arrow"></div><?php echo displayBoxData($lisArray,$m+7);?></div>
                <?php echo displayImagebyIndex($lisArray,$m+7);?>
            </div>
          </div>
          <?php }?>
          
          <?php }?>
          <?php /*end loop slides*/?>
          
        </div>
        
    </div>

 
</div>
