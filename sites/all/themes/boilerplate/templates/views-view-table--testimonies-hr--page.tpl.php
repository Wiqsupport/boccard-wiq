<div class="grey-b relative">
 <div class="hr banner">
      <img class="hidden-xs" src="<?php print base_path().  path_to_theme()?>/images/hr-policy-banner.jpg" />
      <img class="hidden-lg hidden-md" src="<?php print base_path().  path_to_theme()?>/images/talented-mobile.jpg" />
  </div>   
        <a class="absolute talelink" href="<?php print url('recruitment_site');?>"><h1><?php print t("You are talented, join Boccard !");?></h1></a>

<a class="absolute applybtn" href="<?php print url('recruitment_site')?>"><?php print t('APPLY!')?></a>    
<h2 class="title_hr center"><?php print t('Our activities for the careers page')?></h2>
<div class="hr-block hr-page">
    <div class="row-with-bg first">
<?php 
$cnt=count($rows);
foreach ($rows as $row_count => $row):
foreach ($row as $field => $content): 
  switch($field){
    case 'title' : $field_title = $content;break;
    case 'body' : $body = $content;break;
  }
?>
<?php endforeach; ?>  
<div modulo="<?php print $row_count%2 . ' '.$row_count .' '. $cnt?>" class="hr-blk-row hr-blk-row-<?php print $row_count%2 == 0 ? 'left' : 'right'; print($row_count == ($cnt-1)) ? ' last' : '';print($row_count == 0 || $row_count == 1) ? ' first' : '';?>">
<div class="title"><?php print $field_title;?></div>
<div class="body"><?php print $body;?></div>
</div>  
<?php 
if( $row_count%2==1 && $row_count < ($cnt-1) ) print '<div class="clearfix"></div></div><div class="row-with-bg">';
if($row_count == ($cnt-1) ) print '<div class="clearfix"></div></div>';
endforeach; 
?>
    <div class="clearfix"></div>
</div>
</div>
<a class="btn_crv_rof GOBACK" href="<?php print url('hr-policy')?>"><?php print t('GO BACK TO HR POLICY')?></a>
<div class="hire_you">
<div class="title"><?php print t('We want to hire you !');?></div>
<div class="btn-join"><?php print t('Join us')?></div>
</div>