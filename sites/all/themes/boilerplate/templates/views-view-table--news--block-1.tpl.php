<div class="row block-views-news-block">
<div class="swiper-container-mob relative">
<div class="swiper-wrapper">
    
  <?php foreach ($rows as $row_count => $row):
foreach ($row as $field => $content): 
  switch($field){
    case 'field_image' :
      $field_image = $content;
    break;
    case 'field_brief' :
      $field_brief = $content;
    break;
    case 'title' :
      $field_title = $content;
    break;
    case 'nid' :
      $nid = $content;
    break;
  }
?>
<?php endforeach; ?>
 <div class="swiper-slide row-wrap-news row-wrap-news-<?php print $row_count;?> <?php print $row_count==0 ? 'first' : ''; ?>">
     <div class="image relative">
         <div class="absolute maskit">
             <div class="title text-uppercase"><?php print l($field_title,'news');?></div>
             <div class="show-hide-hover">
                <div class="show-hide-hover_inner">
                   <div class="field_brief"><?php print $field_brief;?></div>
                   <div class="more">
                       <div class="row-white-right fl"></div>
                       <a href="<?php print url('news') //print url('node/'.$nid);?>" class="text-uppercase more fl"><?php print t('read more');?></a>
                       <div class="clearfix"></div>
                   </div>
                </div>
             </div>
         </div>
         
         <a href="<?php print url('news')?>"><?php print $field_image;?></a>
         
     </div>
 </div>



<?php endforeach; ?>

    <div class="clearfix"></div>
   </div> 
    
    <div class="swiper-news-mob-button-prev"></div>
    <div class="swiper-news-mob-button-next"></div>
    
 

  </div>    
    
    

    
    
    
    <div class="more-btn text-uppercase"><a href="<?php print url('news')?>"><?php print t("See more news")?></a></div>
</div>
