<?php
  global $user;
    $cnt = count($rows);
    $edit='';
    foreach ($rows as $row_count => $row):
      foreach ($row as $field => $content):
        switch ($field) {
          case 'field_image' :
            $field_image = $content;
            break;
          case 'field_big_image' :
            $field_big_image = $content;
            break;
          case 'field_image_1' :
            $field_image_center = $content;
            break;
          case 'field_image_2' :
            $field_image_2 = $content;
            break;
          case 'field_author' :
            $field_author = $content;
            break;
          case 'title' :
            $field_title = $content;
            break;
          case 'field_youtube_link' :
            $field_youtube_link = $content;
            break;
		  case 'field_youku_link' :
            $field_youku_link = $content;
            break;
          case 'nid' :
            $nid = $content;
            break;
          case 'nid_1' :
            $nid_1 = $content;
            break;
          case 'body' :
            $body = $content;
            break;
          case 'field_country' :
            $field_country = $content;
            break;
        }
        
      if($user->uid==1) $edit = ' <a style="color:red;" href="'.url('node/'.$nid.'/translate').'" target="_blank">[translate]</a>';
      
endforeach;
?>
<li nid="<?php print $nid_1 ;?>" class="row-wrap-testimonials row-wrap-testimonials-<?php print $nid_1 . ($row_count == 0 ? ' first' : '');?>">
    <div class="bg-grey">
      <div class="image"><?php print $field_image_center;?></div>
      <div class="field_title"><?php print $field_title.$edit; ?></div>
<?php if($field_country<>''){?>   <div class="field_country">( <?php print $field_country;?> )</div> <?php }?>
  <?php if($body<>''){?><div class="body my_li_bull">&nbsp;<?php print $body;?> &nbsp;</div> <?php }?>
      <?php if($field_author<>''){?><div class="field_author"><?php print $field_author;?></div><?php }?>
    </div>
      <?php if($field_youtube_link<>''){?>
    <div class="video-wrapper">
          <iframe width="980" height="600" src="https://www.youtube.com/embed/<?php print $field_youtube_link;?>" frameborder="0" allowfullscreen></iframe>
    </div>
      <?php }?>
      <?php if($field_youku_link<>''){?>
    <div class="video-wrapper">
          <iframe width="980" height="600" src="http://player.youku.com/embed/<?php print $field_youku_link;?>" frameborder="0" allowfullscreen></iframe>
    </div>
      <?php }?>
    <?php if($field_big_image<>''){?><div class="field_big_image-wrapper"><?php print $field_big_image;?></div><?php }?>
    </li>
    
  <?php endforeach;  ?>