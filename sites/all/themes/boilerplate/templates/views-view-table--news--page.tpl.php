<?php
// drupal_flush_all_caches(); //       /!\ ligne à supprimer en production /!\
function console($str)
{
    if(isset($_GET['debug']))
    {
        echo $str . "<br />";
    }
}

//Get language
global $language;
$lang = $language->language;

switch($lang)
{
    case 'en':
        $url_lang = 'news-press/';
        break;
    case 'fr':
        $url_lang = 'actualites-presse/';
        break;
    case 'es':
        $url_lang = 'noticias-prensa/';
        break;
    default:
        $url_lang = 'news/';
        break;
}

// Dynamic filter
$final_rows = array();
// Page de marché
if(isset($_GET['marche']))
{
    console('marche');
    foreach ($rows as $row)
    {
        $node = node_load($row['nid']);
        $wrapper = entity_metadata_wrapper('node', $node);
        $marche = $wrapper->field_marche->value();
        if(strpos($_GET['marche'], $marche) !== false)
        {
            console('Node marche : ' . implode('/', array($row['nid'], $marche)));
            $final_rows[$row['nid']] = $row;
        }
    }
}
// Page d'archive par année
if(isset($_GET['y']))
{
    console('annee');
    foreach ($rows as $row)
    {
        $node = node_load($row['nid']);
        $wrapper = entity_metadata_wrapper('node', $node);
        $date = $wrapper->field_date->value();
        $year = date('Y', $date);
        if($_GET['y'] == $year)
        {
            console('Node annee : ' . implode('/', array($row['nid'], $year)));
            $final_rows[$row['nid']] = $row;
        }
    }
}

// Complete
if(isset($_GET['y']) || isset($_GET['marche']))
{

    if(count($final_rows) < 10)
    {
        console('Complete : ' . count($final_rows));


        // Marche 'nucleaire', 'energie', 'petrole_gaz', 'industrie'
        if(strpos($_GET['marche'], 'nucleaire') !== false || strpos($_GET['marche'], 'petrole_gaz') !== false ||
            strpos($_GET['marche'], 'energie') !== false || strpos($_GET['marche'], 'industrie') !== false)
        {
            $good_marche = array('nucleaire', 'energie', 'petrole_gaz', 'industrie');
            foreach ($rows as $row)
            {
                $node = node_load($row['nid']);
                $wrapper = entity_metadata_wrapper('node', $node);
                $marche = $wrapper->field_marche->value();
                if(in_array($marche, $good_marche) && !array_key_exists($row['nid'], $final_rows))
                {
                    if(isset($_GET['y']))
                    {
                        $date = $wrapper->field_date->value();
                        $year = date('Y', $date);
                        if($_GET['y'] == $year)
                        {
                            console('Node + : ' . implode('/', array($row['nid'], $year, $marche)));
                            $final_rows[$row['nid']] = $row;
                        }
                    }
                    else
                    {
                        $final_rows[$row['nid']] = $row;
                        console('Node ++ : ' . implode('/', array($row['nid'])));
                    }
                }
            }
        }

        // Marche alimentation_boisson   brasserie  cosmetique_hygiene  pharma_biotech
        if(strpos($_GET['marche'], 'alimentation_boisson') !== false || strpos($_GET['marche'], 'brasserie') !== false ||
            strpos($_GET['marche'], 'cosmetique_hygiene') !== false || strpos($_GET['marche'], 'pharma_biotech') !== false)
        {
            $good_marche = array('alimentation_boisson', 'brasserie', 'cosmetique_hygiene', 'pharma_biotech');
            foreach ($rows as $row)
            {
                $node = node_load($row['nid']);
                $wrapper = entity_metadata_wrapper('node', $node);
                $marche = $wrapper->field_marche->value();
                if(in_array($marche, $good_marche) && !array_key_exists($row['nid'], $final_rows))
                {
                    if(isset($_GET['y']))
                    {
                        $date = $wrapper->field_date->value();
                        $year = date('Y', $date);
                        if($_GET['y'] == $year)
                        {
                            $final_rows[$row['nid']] = $row;
                            console('Node +++ : ' . implode('/', array($row['nid'], $year, $marche)));
                        }
                    }
                    else
                    {
                        $final_rows[$row['nid']] = $row;
                        console('Node ++++ : ' . implode('/', array($row['nid'])));
                    }
                }
            }
        }

    }


    if(count($final_rows) < 10)
    {
        console('Add boccard news');
        foreach ($rows as $row)
        {
            $node = node_load($row['nid']);
            $wrapper = entity_metadata_wrapper('node', $node);
            $marche = $wrapper->field_marche->value();
            if($marche == 'boccard' && !array_key_exists($row['nid'], $final_rows))
            {
                if(isset($_GET['y']))
                {
                    $date = $wrapper->field_date->value();
                    $year = date('Y', $date);
                    if($_GET['y'] == $year)
                    {
                        console('Node boccard year : ' . implode('/', array($row['nid'], $year, $marche)));
                        $final_rows[$row['nid']] = $row;
                    }
                }
                else
                {
                    $final_rows[$row['nid']] = $row;
                    console('Node boccard : ' . implode('/', array($row['nid'])));
                }
            }
        }
    }
}



// No filter !!
if(empty($final_rows))
{
    console('No filter !');
    $final_rows = $rows;
}

// Pagination
$nb_page = floor(count($final_rows) / 10) + 1;
$current_page = isset($_GET['page']) ? $_GET['page'] : 1;

if($current_page > 1 && $nb_page >= $current_page)
{
    // Je ne sais pas ce que Christian voulais faire ici ???
}


?>

<div class="block_news_tpl_page">
    <div class="banner-locations">
        <div class="banner-locations-inner">
            <div class="locations-ptitle"><?php print t('News & Press')?></div>
        </div>
    </div>
    <div class="news_press_links">
        <div class="wrap wrap_news fl"><?php print l( t('NEWS') , 'news');?></div>
        <div class="wrap wrap_press fl"><?php print l( t('PRESS') , 'press');?></div>
        <div class="clearfix"></div>
    </div>

    <div class="padding_2 boc_news">

        <!-- New news -->

        <div class="row">

            <div class="col-md-12 visible-xs" id="filter_xs">
                <?php echo t("Afficher tous les filtres"); ?>
                <span class="glyphicon glyphicon-triangle-bottom"></span>
            </div>

            <!-- Search menu -->
            <?php
            $selected = isset($_GET['marche']) ? "|" . $_GET['marche'] : "";
            ?>
            <!--    		<div class="col-md-12 nosize nopadding search">-->
            <!--    			--><?php //
            //    				$menus = array(
            //    					'petrole_gaz' 				=> 'Oil & Gas',
            //    					'nucleaire' 				=> 'Nuclear',
            //    					'brasserie' 				=> 'Brewery',
            //    					'alimentation_boisson' 		=> 'Food & Beverage',
            //    					'boccard' 					=> 'Boccard & you'
            //    				);
            //    				foreach ($menus as $slug => $title) :
            //    			?>
            <!--    			--><?php //$is_selected = isset($_GET['marche']) && strpos($_GET['marche'], $slug) !== false; ?>
            <!--    			<a href="--><?php //echo url($url_lang); ?><!--?marche=--><?php //echo $is_selected ? substr(str_replace("||", "|", str_replace($slug, '', $selected)), 1) : $slug . $selected; ?><!--">-->
            <!--	    			<div class="search_bloc boc_--><?php //echo $slug; ?><!-- --><?php //if($is_selected) echo "selected" ?><!--">-->
            <!--		    			<span>--><?php //print t($title);?><!--</span>-->
            <!--	    				<p>--><?php //print t($title);?><!--</p>-->
            <!--	    			</div>-->
            <!--    			</a>-->
            <!--    			--><?php //endforeach; ?>
            <!--    		</div>-->
            <div class="col-md-12 nosize nopadding search">
                <?php
    				$menus = array(
                        'alimentation_boisson' 		=> 'Food & Beverage',
                        'brasserie' 				=> 'Brewery',
                        'cosmetique_hygiene' 	=> 'Cosmetics & Hygiene',
                        'energie' 				=> 'Energy',
                        'industrie' 			=> 'Industries',
                        'nucleaire' 				=> 'Nuclear',
                        'petrole_gaz' 				=> 'Oil & Gas',
                        'pharma_biotech' 		=> 'Pharma & Biotech',
                        'boccard' 					=> 'Boccard & you',
                    );
    				foreach ($menus as $slug => $title) :
    			?>
                <?php $is_selected = isset($_GET['marche']) && strpos($_GET['marche'], $slug) !== false; ?>
                <a href="<?php echo url($url_lang); ?>?marche=<?php echo $is_selected ? substr(str_replace("||", "|", str_replace($slug, '', $selected)), 1) : $slug . $selected; ?>">
                    <div class="search_bloc boc_<?php echo $slug; ?> <?php if($is_selected) echo "selected" ?>">
                        <span><?php print t($title);?></span>
                        <p><?php print t($title);?></p>
                    </div>
                </a>
                <?php endforeach; ?>
                <a href="<?php echo url($url_lang); ?>">
                    <div class="search_bloc boc_cancel">
                        <p><?php print t('CANCEL <br />SELECTION');?></p>
                    </div>
                </a>
            </div>
            <?php /* ajout novembre 2016 */
            if ($current_page>1) {
                for ($dep=0;$dep<($current_page-1)*10;$dep++){array_shift ($final_rows);}
            }
            ?>
            <!-- Left column -->
            <div class="col-md-6 news_left_container nopadding">
                <div class="row">
                    <div class="col-md-12 nopadding">
                        <h3>
                            <?php print l( t('the headlines') , 'news'); ?>
                            <span class="glyphicon glyphicon-menu-down"></span>
                        </h3>
                    </div>

                    <?php
                    $i=0;
                    while($i < count($final_rows) && $i < 3) :
                        $row = current($final_rows);
                        $node = node_load($row['nid']);
                        $wrapper = entity_metadata_wrapper('node', $node);
                        $url = url('node/' . $row['nid'], array('absolute' => TRUE));
                        ?>

                        <div class="bloc_news">
                            <div class="col-md-5 nopadding">
                                <a href="<?php echo $url; ?>"><?php echo $row['field_image']; ?></a>
                            </div>
                            <div class="col-md-7">
                                <span class="boc_marche boc_<?php echo $wrapper->field_marche->value(); ?>"><?php echo isset($row['title_f']) ? $row['title_f'] : ""; ?></span>
                                <a href="<?php echo $url; ?>"><div class="title"><?php echo $row['title']; ?></div></a>
                                <div class="field_brief"><?php echo $row['field_brief']; ?></div>
                                <div class="col-md-12 nopaddingleft">
                                    <div class="body">
                                        <p>
                                            <?php
                                            $fieldView = field_view_field('node', $node, 'body', array('type' => 'text_summary_or_trimmed'));
                                            print render($fieldView);
                                            ?>
                                        </p>
                                    </div>
                                    <?php echo isset($row['view_node']) ? $row['view_node'] : ""; ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="more_button"><a href="<?php echo $url; ?>"><?php echo t('Read more')?><button>Read more</button></a></div>
                            </div>
                        </div>
                        <?php $i++; next($final_rows); endwhile; ?>
                </div>
            </div>

            <!-- Right column -->
            <div class="col-md-6 nopadding news_right_container">
                <div class="row">
                    <?php
                    while($i < count($final_rows) && $i <10) :
                        $row = current($final_rows);
                        $node = node_load($row['nid']);
                        $wrapper = entity_metadata_wrapper('node', $node);
                        $url = url('node/' . $row['nid'], array('absolute' => TRUE));
                        ?>
                        <div class="bloc_news boc_<?php echo $wrapper->field_marche->value(); ?>">
                            <div class="icone boc_<?php echo $wrapper->field_marche->value(); ?>"><?php echo $row['field_marche']; ?></div>
                            <?php
                            $startPos = strpos($row['field_image'], "src=");
                            $img = substr($row['field_image'], $startPos+5, strpos($row['field_image'], '"', $startPos + 6) - $startPos - 5);
                            ?>
                            <a href="<?php echo $url; ?>">
                                <div class="col-md-5 imgBlock" style="background-image: url('<?php echo $img; ?>')"></div>
                            </a>
                            <div class="col-md-6 nopadding content">
                                <a href="<?php echo $url; ?>"><div class="field_brief"><?php echo $row['title']; ?></div></a>
                                <div class="body">
                                    <p>
                                        <?php
                                        //echo $row['field_resume'];
                                        $fieldView1 = field_view_field('node', $node, 'body', array('type' => 'text_summary_or_trimmed'));
                                        print render($fieldView1);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="more_button"><a href="<?php echo $url; ?>"><?php echo t('Read more')?><button>Read more</button></a></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php $i++; next($final_rows); endwhile; ?>
                </div>
            </div>
        </div>
        <!-- /New news -->
        <div class="clearfix"></div>
        <?php /***************** Modif Novembre 2016 ****************/
        $monurl=mb_strtolower(dirname($_SERVER['SERVER_PROTOCOL'])) . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        if (isset($_GET['page'])){
            $monurl=str_replace ( "&page=".$_GET['page'] , '' , $monurl);
            $monurl=str_replace ( "page=".$_GET['page']."&" , '' , $monurl);
        }
        if(isset($_GET['y'])&&$nb_page>1) {
            echo "<div class='text-center'>";
            for ($i=1;$i<$nb_page;$i++){
                if ($i==$current_page){
                    echo $i." &nbsp; ";
                } else {

                    echo "<a href='".$monurl."&page=".$i."'>".$i."</a> &nbsp; ";
                }
            }
            if ($nb_page!=$current_page) {echo "<a href='".$monurl."&page=".$nb_page."'>".$nb_page."</a>";} else {echo "$nb_page";}
            echo "</div>";
        }
        ?>
    </div>
    <div class="archives_bloc">
        <div class="h3"><?php print t('Refer to archived news');?></div>
        <div class="list-archives-year">
            <?php for($i=date('Y'); $i>=2014; $i--) :?>
                <a href="<?php print url($url_lang);?>?y=<?php echo $i; ?>">
                    <div class="year <?php if(isset($_GET['y']) && $_GET['y'] == $i) echo 'selected'; ?>"><?php echo $i; ?></div>
                </a>
            <?php endfor; ?>
        </div>
    </div>

</div>
