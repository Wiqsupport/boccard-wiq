<div class="relative">
  <div class="hr banner" id="js_testi_bann">
      <img class="hidden-xs" src="<?php print base_path().  path_to_theme()?>/images/hr-policy-banner.jpg" />
      <img class="hidden-lg hidden-md hidden-sm" src="<?php print base_path().  path_to_theme()?>/images/talented-mobile.jpg" />
  </div>
    <div class="hidden-lg hidden-md grey-bg-mobile"></div>
    <a class="absolute talelink" href="<?php print url('recruitment_site');?>"><h1><?php print t("You are talented, join Boccard !");?></h1></a>
    
<div class="disk_open_close hr-policy absolute"></div>


<a class="absolute applybtn" href="<?php print url('recruitment_site')?>"><?php print t('APPLY!')?></a>

<div id="profile-first-set">
<div id="pannel-job-profiles-4" class="relative">
<div class="close_profile_4 absolute"></div>
<h2 class="title_hr"><?php print t('Why you should join us ?')?></h2>

<div class="invoke invoke-0">
<?php
$block = module_invoke('views', 'block_view', 'testimonies_hr-block');
print render($block['content']);
?>
</div>
  <div class="wrap-btns-crv" style="text-align: center;">
<!--  <a class="btn_crv_rof btn_crv_rof_left" href="<?php print url('hr-policy-job-profile')?>">PROFILES</a>-->
<!--  <div class="btn_crv_rof btn_crv_rof_right fr">Explore Boccard in 120 s</div>-->
  <div class="clearfix"></div>
  </div>
<div class="clearfix"></div>
</div>
<div class="invoke invoke-1">
<?php
$block = module_invoke('views', 'block_view', 'testimonies_hr-block_1');
print render($block['content']);
?>
</div>
<div class="hire_you">
<div class="title"><?php print t('We want to hire you !');?></div>
<a class="btn-join" href="<?php print url('recruitment_site')?>"><?php print t('Join us')?></a>
</div>
</div>



  <div class="invoke invoke-2">
      <h2 class="invoke-2-title"><?php print t('Testimonies');?></h2>
  <?php
  $block = module_invoke('views', 'block_view', 'testimonies_hr-block_2');
  print render($block['content']);
  ?>
  </div>


<div class="hire_you hire_you2">
<div class="title"><?php print t('We want to hire you !');?></div>
<a class="btn-join" href="<?php print url('recruitment_site')?>"><?php print t('Join us')?></a>
</div>

</div>