<div id="block-views-successstory-block">
    <h2 class="sc-block-title"><?php print t('Success stories'); ?></h2>
    <div class="wrapper">
        <div class="jcarousel-wrapper">
            <div class="jcarousel">
<ul id="slider_logos">
    <?php
    $as_string = '';
    $cnt = count($rows);
    global $user;
    foreach ($rows as $row_count => $row):
      foreach ($row as $field => $content):
        switch ($field) {
          case 'field_image' :
            $field_image = $content;
            break;
          case 'field_big_image' :
            $field_big_image = $content;
            break;
          case 'field_image_1' :
            $field_image_center = $content;
            break;
          case 'field_author' :
            $field_author = $content;
            break;
          case 'title' :
            $field_title = $content;
            break;
          case 'field_youtube_link' :
            $field_youtube_link = $content;
            break;
		  case 'field_youku_link' :
            $field_youku_link = $content;
            break;
          case 'nid' :
            $nid = $content;
            break;
          case 'body' :
            $body = $content;
            break;
          case 'field_country' :
            $field_country = $content;
            break;
        }
              if($user->uid==1) $edit = ' <a style="color:red;" href="'.url('node/'.$nid.'/translate').'" target="_blank">[translate]</a>';

      endforeach;
    ?>
                    
                    
    <li class="<?php if($row_count==0) print 'first_trigger_li'?>" rel="<?php print $nid ?>">
                          <div class="wrapper">
                            <div class="cell">
                                <?php print $field_image.$edit ?>
                            </div>
                          </div>
                        </li>
                      
    <?php endforeach;?>
                      
                </ul>
                <div class="clearfix"></div>
            </div>
            <?php if( $cnt >4){?><?php }?>
              <a href="#" class="jcarousel-control-prev"></a>
              <a href="#" class="jcarousel-control-next"></a>
            
            
        </div>
    </div>
</div>