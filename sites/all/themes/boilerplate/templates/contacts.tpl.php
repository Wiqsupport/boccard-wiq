<div class="banner-locations">
<div class="banner-locations-inner">
<h1 class="locations-ptitle"><?php print t('Contact Us')?></h1>
</div>
</div>

<div class="wrapper-grey">
  
<h2 class="you_have_big_title_contacts"><?php print t("You have a project, Contact Us !")?></h2>

<?php 
$getFrom = drupal_get_form("custom_contact_form");
print render($getFrom);

?>

</div>