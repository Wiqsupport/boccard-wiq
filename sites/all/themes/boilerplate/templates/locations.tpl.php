<div class="banner-locations">
<div class="banner-locations-inner">
<h1 class="locations-ptitle"><?php print t('Locations')?></h1>
</div>
</div>
<div id="map-canvas"></div>
<div id="country_dropdown">
    <select name="country_input" type="text" id="country_input">
        <option value="All"><?php print t("Choose your country")?></option>
      <?php foreach($countries as $code2=>$country){?>
        <option value="<?php print $code2?>"><?php print $country?></option>
      <?php }?>
    </select>
</div>
<div id="wrapper-contact-list"></div>