<div class="margin_bottom_s">
<div class="block_interesting_tpl_block" id="masonry">
<?php 
//global $user;
$edit ='';
foreach ($rows as $row_count => $row):
foreach ($row as $field => $content): 
  switch($field){
    case 'field_image' :
      $field_image = $content;
    break;
    case 'field_number' :
      $field_number = $content;
    break;
    case 'body' :
      $body = $content;
    break;
    case 'nid' :
      $nid = $content;
    break;
  }
  if($user->uid==1) $edit = '<div class="node_edit absolute">'.l('edit' , 'node/'.$nid.'/edit' , array(
    'query'=>array('destination'=>  'node/607' )
  ) ).'</div>';
  ?>
<?php endforeach; ?>
 <div class="rowif rowif-<?php print $nid?> fl">
     <div class="rowif_inner">
         <div class="icon_grey"><?php print $field_image;?></div>
         <div class="body_number"><span><?php print $field_number?></span><?php print '&nbsp'.strip_tags($body,'<br><br /><br/>').$edit;?></div>
     </div>
 </div>
<?php endforeach; ?>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
