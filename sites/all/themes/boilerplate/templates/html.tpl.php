<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>> <!--<![endif]-->
<head>
    <?php print $head; ?>
    <?php /*<meta property="og:image" content="<?php echo file_create_url($node->field_image['und'][0]['uri']); ?>" /><link rel="image_src" href="<?php echo file_create_url($node->field_image['und'][0]['uri']); ?>" />*/ ?>

    <title><?php print $head_title; ?></title>
    <?php //if (!$is_admin){?>
    <link rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/css/bootstrap.min.css">
    <?php //} ?>
    <?php print $styles; ?>
    <?php print $scripts;?>

    <?php if( drupal_is_front_page() ){?>
        <script src="<?php print base_path() . path_to_theme() ?>/js/jquery.knob.js"></script>
        <script src="<?php print base_path() . path_to_theme() ?>/js/jquery.throttle.js"></script>
        <!--    <script src="<?php print base_path() . path_to_theme() ?>/js/jquery.classycountdown.js"></script>-->
    <?php } ?>

    <?php
    // tarteaucitron.js languages support
    global $language;
    $tarteaucitron_supported_lang = ["cs","en","fr","es","it","de","nl","pt","pl","ru","el"];
    $tarteaucitron_display_lang = in_array($language->language, $tarteaucitron_supported_lang) ? $language->language : "en";
    $privacy_url_translations = translation_path_get_translations(drupal_get_normal_path("data-protection-policy","en"));
    $language_shortcode = $language->language == "zh-hans" ? "cn" : $language->language;
    ?>
    <script type="text/javascript" src="<?php print base_path() . path_to_theme() ?>/js/libs/tarteaucitron/tarteaucitron.js"></script>
    <script type="text/javascript">
        var tarteaucitronForceLanguage = '<?php echo $tarteaucitron_display_lang; ?>';
        tarteaucitron.init({
            "privacyUrl": '<?php echo $language_shortcode.'/'.$privacy_url_translations[$language->language]; ?>',
            "hashtag": "#tarteaucitron",
            "cookieName": "tarteaucitron",
            "orientation": "bottom",
            "showAlertSmall": false,
            "cookieslist": true,
            "adblocker": false,
            "AcceptAllCta" : true,
            "highPrivacy": false,
            "handleBrowserDNTRequest": false,
            "removeCredit": true,
            "moreInfoLink": false,
            "useExternalCss": false,
        });
    </script>

</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>
<?php if (!$is_admin) { ?>
    <script src="<?php print base_path() . path_to_theme() ;?>/js/bootstrap.js"></script>

<?php } ?>
<script type="text/javascript">
    tarteaucitron.user.analyticsUa = 'UA-35681655-2';
    (tarteaucitron.job = tarteaucitron.job || []).push('analytics');
    (tarteaucitron.job = tarteaucitron.job || []).push('youtube');
</script>

</body>
</html>