<div class="footer_logo fl"><img src="<?php print base_path() . path_to_theme() ?>/images/logo-footer.png" /></div>  
<div class="social-btns fr">
    <ul class="row">
        <li class="fb"><a href="https://www.facebook.com/pages/Boccard/151778961527405" title="" target="_blank" class=""></a></li>    
        <li class="in"><a href="https://www.linkedin.com/company/boccard?trk=biz-companies-cym" title="" target="_blank" class=""></a></li>    
        <li class="tw"><a href="https://twitter.com/Boccard_" title="" target="_blank" class=""></a></li>    
        <li class="ut"><a href="https://www.youtube.com/user/BOCCARDfoodpharma" title="" target="_blank" class=""></a></li>    
    </ul>
</div>
<div class="clearfix"></div>


<div class="sitemap_linear">
    <?php
    global $language;
$lang = $language->language;

switch($lang){
  case 'fr' : $mnu_name = "menu-main-menu-fr";break;
  case 'es' : $mnu_name = "menu-main-menu-es";break;
  case 'zh-hans' : $mnu_name = "menu-main-menu-cn";break;
  case 'pl' : $mnu_name = "menu-main-menu-pl";break;
  case 'ru' : $mnu_name = "menu-main-menu-ru";break;
  default :$mnu_name = "main-menu";break;
}
    $full_menu=menu_tree_output(menu_tree_all_data($mnu_name, null,2));
    
    $array_f=recur_mnu($full_menu);
    print $array_f;
    ?>
  <div class="clearfix rl"></div>
</div>


<div id="footer-menu" class="fr">
    <?php
    $footer_menu=menu_tree_output( menu_tree_all_data("menu-footer") );
    print  drupal_render($footer_menu);
    ?>
</div>
<div class="clearfix"></div>