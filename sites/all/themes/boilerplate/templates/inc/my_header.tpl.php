<div class="row wrap-collls">

    <div class="logo col-xs-5 col-lg-6 col-sm-4 sww">
        <a href="<?php print url('<front>'); ?>">
            <img src="<?php print base_path() . path_to_theme() ?>/images/logo.png" />
        </a>
    </div>

    <div class="col-xs-5 col-lg-3 col-sm-4  swl">
        <div class="search_container">
            <input class="custom_search_input fl" type="text" />
            <input class="custom_search_submit fr" type="submit" value="submit" />
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="hidden-xs col-lg-2 col-sm-3">
        <div class="social-btns fr social-btns-header top">
            <ul class="row">
                <li class="fb"><a href="https://www.facebook.com/pages/Boccard/151778961527405" title="" target="_blank" class=""></a></li>
                <li class="in"><a href="https://www.linkedin.com/company/boccard?trk=biz-companies-cym" title="" target="_blank" class=""></a></li>
                <li class="tw"><a href="https://twitter.com/Boccard_" title="" target="_blank" class=""></a></li>
                <li class="ut"><a href="https://www.youtube.com/user/BOCCARDfoodpharma" title="" target="_blank" class=""></a></li>
            </ul>
        </div>
    </div>
    <div class="col-xs-2 col-lg-1 relative col-sm-1 sws">
        <div id="menu-toggle" class="menu-toggle">
            <span class="i_bar_btn">
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
            </span>
            <span class="text text-uppercase"><?php print t("MENU")?></span>
        </div>

        <div id="menu_wrapper" class="absolute">
            <div id="menu_wrapper_inner" class="relative">
                <div id="close_menu" class="absolute close_menu"></div>
                <div id="pane2" class="scroll-pane noscrollwindow">
                    <div class="white_cover"></div>
                    <?php
                    global $language;
                    $lang = $language->language;
                    switch($lang){
                        case 'fr' : $mnu_name = "menu-main-menu-fr";break;
                        case 'es' : $mnu_name = "menu-main-menu-es";break;
                        case 'zh-hans' : $mnu_name = "menu-main-menu-cn";break;
                        case 'pl' : $mnu_name = "menu-main-menu-pl";break;
                        case 'ru' : $mnu_name = "menu-main-menu-ru";break;
                        default :$mnu_name = "main-menu";break;
                    }


                    $full_menu=menu_tree_output(menu_tree_all_data($mnu_name, null,5));
                    print str_replace('span','a',drupal_render($full_menu));


                    ?>

                    <div class="search_container search_container_mobile hidden-lg hidden-md hidden-sm">
                        <input class="custom_search_input mobile" type="text" />
                        <input class="custom_search_submit" type="submit" value="submit" />
                    </div>

                    <div id="switch_lang">
                        <div class="Languages fl"><?php print t("Languages")?></div>
                        <div class="fl cicles">
                            <?php $block = module_invoke('locale', 'block_view', 'language');
                            print $block['content'];
                            ?>
                            <div class="clearfix"></div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="others_menu">
                        <div class="bocpredict">
                            <div class="fl bg"></div>
                            <div class="txt fl"><a target="_blank" href="https://www.bocpredict.com/"><?php print "BocPredict" ?></a></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="private_access">
                            <div class="fl bg"></div>
                            <div class="txt fl"><a target="_blank" href="http://www.boctrack.com/n_cli_login.php"><?php print "BocTrack" //t("Private Access")?></a></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="menu_years_100">
                            <div class="fl bg"></div>
                            <div class="txt fl"><a target="_blank" href="http://100years.boccard.com"><?php print t("Boccard First 100 years") ?></a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="social-btns social-btns-header">
                        <ul class="row">
                            <li class="fb"><a href="https://www.facebook.com/pages/Boccard/151778961527405" title="" target="_blank" class=""></a></li>
                            <li class="in"><a href="https://www.linkedin.com/company/boccard?trk=biz-companies-cym" title="" target="_blank" class=""></a></li>
                            <li class="tw"><a href="https://twitter.com/Boccard_" title="" target="_blank" class=""></a></li>
                            <li class="ut"><a href="https://www.youtube.com/user/BOCCARDfoodpharma" title="" target="_blank" class=""></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>