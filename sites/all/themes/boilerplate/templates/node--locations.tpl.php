<div class="banner-locations">
<div class="banner-locations-inner">
<h1 class="locations-ptitle"><?php print t('Locations')?></h1>
</div>
</div>


<div id="template_location">
    <?php
    global $language;
    $lang = $language->language;
    ?>
    <div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">
        <div class="node-inner">
            <div id="wrapper-contact-list">
                <div class="html_contacts_popup_listing 513 last">
                    <div class="inner">
                        <div class="fl left-one">
                            <div class="popc_title"><?php print $node->title?></div>
                            <div class="popc_field_adresse"><?php print $node->field_adresse['und'][0]['value'] ?></div>
                            <div class="popc_field_adresse_postale"><?php print $node->field_adresse_postale['und'][0]['value'] ?></div>
                            <div class="popc_field_country"><?php print $node->field_country['und'][0]['value'] ?></div>
                            <div class="popc_siege_social"><?php print $node->field_extra['und'][0]['value'] ?></div>
                        </div>
                        <div class="fr right-one">
                            <div class="picto popc_field_tel"><?php print $node->field_tel['und'][0]['value'] ?></div>
                            <div class="picto popc_field_fax"><?php print $node->field_fax['und'][0]['value'] ?></div>
                            <div class="picto popc_field_email"><a href="mailto:<?php print $node->field_email['und'][0]['value'] ?>"><?php print $node->field_email['und'][0]['value'] ?></a></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>