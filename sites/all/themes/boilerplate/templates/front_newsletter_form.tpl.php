<div class="absolute popup_nl_jet">
<div>
    <form class="form_mail_jet" id="form_mail_jet" action="<?php print url('custom_add_contact_to_mailjet_list')?>" method="POST">
    <div class="relative">
    <div class="close_pop_jet absolute"></div>
    <div class="titlepop"><?php print t("Choose your newsletter");?></div>    
    <div class="nls_list">
        <div class="radios_all">
              <input name="mail_hidden" id="mail_hidden" type="hidden" />
              <input value="<?php print $nls[0]['id']?>" name="categ_hidden" id="categ_hidden" type="hidden" />
        <?php foreach($nls as $k=>$nl){ ?>
          <div class="zradio" rel="<?php print $nl['id']?>">
              <div class="crad crad-<?php print $nl['id']?> fl"></div>
              <div class="zlabel fl"><?php print $nl['subject'];?></div>
              <div class="clearfix"></div>
          </div>
        <?php } ?>
        </div>
    </div>
    <input type="submit" value="OK" class="btn_subs ok" />
</div>
</form>
</div>
</div>
<div class="rub_title"><?php print t("Subscribe to our newsletters");?></div>
<div class="form_wrapper">
    <div class="form-item">
        <input name="email" type="text" class="mail_input fl" placeholder="<?php print t("Email");?>" />
    </div>
    <div class="form-item">
        <input type="checkbox" id="rgpd" name="rgpd" class="rgpd_input form-checkbox" required>
        <label class="option" for="edit-rgpd"><?php print t('By checking this box, you agree to receive our newsletter and news about Boccard. You can at any time and very easily unsubscribe by clicking on the unsubscribe links at the bottom of each newsletter.
For more information, we invite you to consult our <a href="/en/data-protection-policy" target="_blank">data protection policy</a>.'); ?></label>
    </div>
    <div class="form-item">
        <div class="submit-wrapper"><input value="<?php print t("> OK");?>" type="submit" class="mail_submit fr" /></div>
    </div>
<div class="clearfix"></div>
</div>