<?php
global $language;
global $user;

$lang = $language->language;
$node_ref = $node->field_banner_reference['und'][0]['node'];

$node_files = isset($node->field_files['und']) ? $node->field_files['und'] : [];
$path = 'node/' . $node->nid;
$parent = menu_link_get_preferred($path);

switch($lang){
  case 'fr' : $mnu_name = "menu-main-menu-fr";break;
  case 'es' : $mnu_name = "menu-main-menu-es";break;
  case 'zh-hans' : $mnu_name = "menu-main-menu-cn";break;
  case 'pl' : $mnu_name = "menu-main-menu-pl";break;
  case 'ru' : $mnu_name = "menu-main-menu-ru";break;
  default :$mnu_name = "main-menu";break;
}
//d($parent);

$mtree = ca(menu_tree($mnu_name))[$parent['p1']]['#below'];//[386]['#below']
  
$link_contact = $parent['p2'];
  
$p = 'p2';
if ( $parent['p4']<> 0 || $parent['p5']<>0 ){
  $p = 'p3';
  $mtree = ca(menu_tree($mnu_name))[$parent['p1']]['#below'][$parent['p2']]['#below'];//[386]['#below']
  
  $link_contact = $parent['p3'];
  if(in_array( $parent['p3'] , array( '390' ))) $link_contact = $parent['p4'];
}

//d($mtree);

$dropdown_title = $mtree[$parent[$p]]['#title'];

//global $user;if ($user->uid==1) d($mtree[$parent['p2']]);

$items=array();
$bloItems=array();
foreach( $mtree[$parent[$p]]['#below'] as $k=>$below){

  $items=array();
  if(sizeof($below['#below'])>0 )
  {
      $bloItems[$k]['blue_title'] = $below['#title'];
      foreach($below['#below'] as $bb){
          if($bb['nid'] == arg(1))
              $items[] ='<span class="close_categ">'.$bb['#title'].'</span>';          
          else
          $items[] =l($bb['#title'], $bb['#href']/*, array('query' => array('o' => 1))*/);
      }
      $bloItems[$k]['items']=$items;
  }
  else
  {
    if($below['nid'] == arg(1))
        $items[] ='<span class="close_categ">'.$below['#title'].'</span>';          
    else
        $items[] =l($below['#title'], $below['#href']/*, array('query' => array('o' => 1))*/);
        $bloItems['onelev']='onelev';
        $bloItems['items'][]=$items;
  }
}


$ntran = translation_node_get_translations($node_ref->tnid);
$node_ref_lang_nid = $ntran['en']->nid ;

?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes;print " node_ref_nid_" . $node_ref_lang_nid; print ' depth-'.$parent['depth'];?>">
    <div class="node-inner">
        <div class="banner relative" id="banner_<?php print $node_ref_lang_nid;?>">
<?php 
$fid = isset($node->field_image['und'][0]['fid']) ? $node->field_image['und'][0]['fid'] : NULL;
// if field image is empty, take field_banner_reference
if ($fid == NULL) {
    $fid = isset($node_ref->field_image['und'][0]['fid']) ? $node_ref->field_image['und'][0]['fid'] : NULL;
}
$file = file_load($fid);
$banner_url= file_create_url($file->uri);
?>
            
            <img class="hide-<?php print $node_ref_lang_nid;?>" src="<?php print $banner_url?>" />
            <div class="absolute absolute-dropdown_title_brief hidden-xs">
                <div class="dropdown">
                    
                        <div class="txt fl">Boccard <?php print $dropdown_title ?></div>
                    
                    <div class="bg fl"></div>
                    <div class="clearfix"></div>
                </div>
                <h1 class="title n-<?php print $node->nid; ?>"><?php print $node->title; ?></h1>
                <!--h2 class="title n-<?php print $node->nid; ?>"><?php print $node->title; ?></h2-->
                <div class="brief"><?php print (isset($node->field_brief['und'][0]['value']) && $node->field_brief['und'][0]['value'] <> '') ? $node->field_brief['und'][0]['value'] : $node_ref->field_brief['und'][0]['value'] ; ?></div>
            </div>
            <div class="disk_open_close<?php if (isset($_GET['o']) && $_GET['o'] == 1) print' trigger_desc' ?> absolute hidden-xs"></div>
            <a href="http://www.meura.com/" target="_blank" class="absolute meura_link"></a> 
        </div>
        
   
<div class="mobile-dropdown_title_brief relative hidden-lg hidden-md hidden-sm">
  <div class="dropdown">
    <div class="txt fl">Boccard <?php print $dropdown_title ?></div>
    <div class="bg fl"></div>
    <div class="clearfix"></div>
  </div>
  <h1 class="title n-<?php print $node->nid; ?>"><?php print $node->title; ?></h1>
  <!--h2 class="title n-<?php print $node->nid; ?>"><?php print $node->title; ?></h2-->
  <div class="brief"><?php print (isset($node->field_brief['und'][0]['value']) && $node->field_brief['und'][0]['value'] <> '') ? $node->field_brief['und'][0]['value'] : $node_ref->field_brief['und'][0]['value'] ; ?></div>
<div class="disk_open_close<?php if (isset($_GET['o']) && $_GET['o'] == 1) print' trigger_desc' ?> absolute hidden-lg hidden-md"></div>        
</div>
        
        <div class="categories" id="categories-pannel">
            <div class="categories_inner relative">
                <div class="categories_inner_close_desc"><a rel="nofollow" class="active" href="#">close</a></div>
                <div class="cat-pixs-top"></div>
                  <div class="center cmza">
                    <?php
                    if(!isset($bloItems['onelev']) || $bloItems['onelev'] != 'onelev'){
                    foreach($bloItems as  $kf=>$vf){
                      if(sizeof($vf['items'])>0){?>
                          <div class="fb-wrapper food-wrapper fl">
                          <div class="title2 hasChildren fl"><?php print $vf['blue_title'];?></div>
                          <div class="fl childs"><?php print theme('item_list', array('items' => $vf['items']));?></div>
                          </div>
                    <?php }}}else{?>
                          <div class="nochpt"><?php print theme('item_list', array('items' => $bloItems['items']));?></div>
                    <?php }?>
                          <div class="clearfix"></div>
                  </div>
                          
                <div class="clearfix"></div>
                <div class="cat-pixs-bot"></div>
            </div>
        </div>
        <div class="description relative">
            <div class="description_inner">
                <div class="close_desc"><?php print l('close', 'node/' . $node->nid) ?></div>
                <div class="top_row">
                    <div class="rrow top_row_inner">
                        <div class="fl field_your_needs">
                            <div class="title txt_18"><?php print t('Your needs'); ?></div>
                            <div class="text"><?php print $node->field_your_needs['und'][0]['value']; ?></div>
                            <?php
                            
                            if (isset($node->field_pdf1['und'][0]['fid']) && $node->field_pdf1['und'][0]['fid']) {
                              foreach ($node->field_pdf1['und'] as $elem) {
                                $file_uri = file_create_url( $elem['uri'] );
                                ?>
<div class="pdf pdf1">
<a class="bg" href="<?php print url($file_uri); ?>">
  <?php print $elem['description'] <>''?$elem['description'] : t('Download our brochures'); ?>
</a>
</div>
                            
                            
  <?php }} ?>
  <?php if (isset($node->field_photo1['und'][0]['uri']) && $node->field_photo1['und'][0]['uri']) {?>
        <div class="image_read_more">
          <?php print theme("image_style",array(
            "style_name"=>"read_more" , 
            "path"=>$node->field_photo1['und'][0]['uri'],
            'alt' => $node->field_photo1['und'][0]['alt'] ,
            'title' => $node->field_photo1['und'][0]['title']
              ))?>
        </div>                   
  <?php }?>
                            
                        </div>
                        <div class="fr field_our_added_value">
                            <div class="title txt_18"><?php print t('Our added value'); ?></div>
                            <div class="text"><?php print $node->field_our_added_value['und'][0]['value']; ?></div>
                            <?php
                            if (isset($node->field_pdf3['und'][0]['fid']) && $node->field_pdf3['und'][0]['fid']) {
                              foreach ($node->field_pdf3['und'] as $elem) {
$file_uri = file_create_url( $elem['uri'] );
                                
                                ?>
<div class="pdf pdf3">
<a class="bg" href="<?php print url($file_uri); ?>">
  <?php print $elem['description'] <>''?$elem['description'] : t('Download our brochures'); ?>
</a>
</div>
                            
  <?php }} ?>  
<?php if (isset($node->field_photo3['und'][0]['uri']) && $node->field_photo3['und'][0]['uri']) {?>
        <div class="image_read_more">
          <?php print theme("image_style",array(
            "style_name"=>"read_more" , 
            "path"=>$node->field_photo3['und'][0]['uri'],
            'alt' => $node->field_photo3['und'][0]['alt'] ,
            'title' => $node->field_photo3['und'][0]['title']
              ))?>
        </div>                   
  <?php }?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>


                <div class="bottom_row">
                    <div class="rrow bott_row_inner">
                        <div class="fl field_our_solutions">
                            <div class="title txt_18"><?php print t('Our solutions'); ?></div>
                            <div class="text"><?php print $node->field_our_solutions['und'][0]['value']; ?></div>
                            <?php
                            if (isset($node->field_pdf2['und'][0]['fid']) && $node->field_pdf2['und'][0]['fid']) {
                              foreach ($node->field_pdf2['und'] as $elem) {                               
                              $file_uri = file_create_url( $elem['uri'] );?>
<div class="pdf pdf2">
<a class="bg" href="<?php print url($file_uri); ?>">
  <?php print $elem['description'] <>''?$elem['description'] : t('Download our brochures') ; ?>
</a>
</div>
                            
  <?php }} ?>  
<?php if (isset($node->field_photo2['und'][0]['uri']) && $node->field_photo2['und'][0]['uri']) {?>
        <div class="image_read_more">
          <?php print theme("image_style",
              array("style_name"=>"read_more" , 
                "path"=>$node->field_photo2['und'][0]['uri'],
                'alt' => $node->field_photo2['und'][0]['alt'] ,
                'title' => $node->field_photo2['und'][0]['title']
              ))?>
        </div>                   
  <?php }?>
                        </div>
                        <div class="fr field_our_technologies_services">
                            <div class="title txt_18"><?php print t('Our products & services'); ?></div>
                            <div class="text">
                            <?php print $node->field_our_technologies_services['und'][0]['value']; ?>

                            </div>
                                <?php
                                if (isset($node->field_pdf4['und'][0]['fid']) && $node->field_pdf4['und'][0]['fid']) {
                                  foreach ($node->field_pdf4['und'] as $elem) {
                                    $file_uri = file_create_url( $elem['uri'] );
                                
                                ?>
<div class="pdf pdf4">
<a class="bg" href="<?php print url($file_uri); ?>">
  <?php print $elem['description'] <>''?$elem['description'] : t('Download our brochures'); ?>
</a>
</div>
                            
                              <?php }} ?>  
<?php if (isset($node->field_photo4['und'][0]['uri']) && $node->field_photo4['und'][0]['uri']) {?>
        <div class="image_read_more">
          <?php print theme("image_style",array("style_name"=>"read_more" , 
            "path"=>$node->field_photo4['und'][0]['uri'],
                'alt' => $node->field_photo4['und'][0]['alt'] ,
                'title' => $node->field_photo4['und'][0]['title']))?>
        </div>                   
  <?php }?>  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

				<div class="marches-encart"><a target="_blank" href="http://www.boctrack.com/n_cli_login.php"></a></div>
                <div class="clearfix"></div>

      <div class="contact_have_proj">
          <div class="txt_big"><?php print t("Contact Us !");?></div>
          <div class="main_titl_ico_wrap">
              <div class="main_title"><?php print $dropdown_title;?></div>
              <div class="main_ico <?php print strtolower(drupal_clean_css_identifier($dropdown_title)).' nid-olang-'.$node_ref_lang_nid;?>"></div>
          </div>
          <a rel="nofollow" href="<?php print url('contacts').'/'.$link_contact.'/'.$node->nid; ?>" class="main_btn_cont text-uppercase"><?php print t("Contact FORM")?></a>
      </div>
            </div>
        </div>
        
        
        
<?php
$block = module_invoke('views', 'block_view', 'successstory-block_2');
print render($block['content']);
?>
<?php
$block = module_invoke('views', 'block_view', 'successstory-block');
print render($block['content']);
?>


<div class="contact_have_proj contact_have_proj2">
            <div class="txt_big"><?php print t("Contact Us !");?></div>
            <div class="main_titl_ico_wrap">
                <div class="main_title"><?php print $dropdown_title; ?></div>
                <div class="main_ico <?php print strtolower(drupal_clean_css_identifier($dropdown_title)).' nid-olang-'.$node_ref_lang_nid;?>"></div>
            </div>
            <a rel="nofollow" href="<?php print url('contacts').'/'.$link_contact.'/'.$node->nid; ?>" class="main_btn_cont text-uppercase"><?php print t("Contact FORM")?></a>
        </div>



    </div>
</div>

