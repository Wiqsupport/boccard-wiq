<div id="block-views-successstory-block-2" class="relative">
  
    <h2 class="sc-block-title"><?php print t('Success stories'); ?></h2>
  <div class="swiper-container-ss s1">
<div class="swiper-wrapper">
    <?php
    $cnt = count($rows);
    foreach ($rows as $row_count => $row):
      foreach ($row as $field => $content):
        switch ($field) {
          case 'field_image' : $field_image = $content;break;
          case 'nid_1' : $nid_1 = $content;break;
        }
      endforeach;
    if($nid_1 <> '') $field_image_arra[$nid_1]=$field_image;
    endforeach; 
    $n=0;
    foreach ( $field_image_arra as $k=>$v){
    ?>
    <div class="swiper-slide <?php if ($n == 0) print 'first_trigger_li' ?>" rel="<?php print $k ?>">
      <div class="wrapper">
        <div class="cell"><?php print $v ?></div>
      </div>
    </div>
    <?php $n++;}?>
  </div>
  </div>
<?php if ($cnt > 5) { ?><?php } ?>
<a class="swiper-button-ss-prev"></a>
<a class="swiper-button-ss-next"></a>
  
</div>