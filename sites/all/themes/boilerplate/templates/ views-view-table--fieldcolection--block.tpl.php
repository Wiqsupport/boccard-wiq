<div class="swiper-container">
<div class="swiper-wrapper">
<?php foreach ($rows as $row_count => $row):
foreach ($row as $field => $content): 
  switch($field){
    case 'field_image_mobile' : $field_image_mobile = $content;break;
    case 'field_image' : $field_image = $content;break;
    case 'field_brief' : $field_brief = $content;break;
    case 'title' : $field_title = $content;break;
    case 'nid' : $nid = $content;break;
  }
?>
<?php endforeach; ?>
 <div class="swiper-slide relative">
         <div class="absolute text_on_banner text_on_banner-<?php print $nid?>">
             <div class="big"><?php print $field_title;?></div>
             <div class="sml"><?php print $field_brief;?></div>
          </div>
     <div class="hidden-lg hidden-md"><?php print $field_image_mobile;?></div>
     <div class="hidden-xs"><?php print $field_image;?></div>
 </div>
<?php endforeach; ?>
</div>
<!--<div class="swiper-pagination"></div>-->
</div>
