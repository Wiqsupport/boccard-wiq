<div class="hr-block">
    <div class="row-with-bg first">
<?php 
$cnt = count($rows);
foreach ($rows as $row_count => $row):
foreach ($row as $field => $content): 
  switch($field){
    case 'title' : $field_title = $content;break;
    case 'body' : $body = $content;break;
    case 'nid' : $nid = $content;break;

  }
?>
<?php endforeach; ?>
        <div nid="<?php print $nid;?>" modulo="<?php print $row_count%2 . ' '.$row_count .' '. $cnt?>" class="hr-blk-row hr-blk-row-<?php print $row_count%2 == 0 ? 'left' : 'right'; print($row_count == ($cnt-1)) ? ' last' : '';print($row_count == 0 || $row_count == 1) ? ' first' : '';?>">
          <?php if(! in_array($nid , array(1003 ,1999, 1504 , 1998, 2984, 3700))){?>
            <div class="title"><?php print $field_title?></div>
            <div class="body"><?php print $body;?></div>
          <?php }else{?>
            <div id="discover_boccardJJ">
                <div class="discover_boccard_new_btn">
                    <div class="bg"></div>
                    <div class="txt"><?php print t("Explore Boccard's history<br>in 120 sec");?></div>
                </div>
            </div>
          <?php }?>
          
       </div>
  
    
<?php 


if( $row_count%2==1 && $row_count < ($cnt-1) ) print '<div class="clearfix"></div></div><div class="row-with-bg">';
if($row_count == ($cnt-1) ) print '<div class="clearfix"></div></div>';

endforeach; ?>
</div>