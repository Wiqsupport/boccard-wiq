  <?php if( $set=="mobile" ){?>
<h2 class="hidden-lg psps">  <?php print t("Plant solutions<br>& Process solutions");?></h2>
    <?php }?>
<?php
global $language;
$lang = $language->language;
switch($lang){
  case 'fr' : $mnu_name = "menu-main-menu-fr";break;
  case 'es' : $mnu_name = "menu-main-menu-es";break;
  case 'zh-hans' : $mnu_name = "menu-main-menu-cn";break;
  case 'pl' : $mnu_name = "menu-main-menu-pl";break;
  case 'ru' : $mnu_name = "menu-main-menu-ru";break;
  default :$mnu_name = "main-menu";break;
}
$mtree = menu_tree($mnu_name);
$mtitle= isset($mtree[$mid]['#title']) ? $mtree[$mid]['#title'] : "";
$paretnClass = "";
$subs=array();
//d($set);
if( $set!='mobile' && $set!='services_mobile' ){ ?>
<div class="setHH-<?php print $set;?>">
<div id="wrapper-<?php print $style;?>" class="relative">
<div class="absolute white-layer-trick white-layer-trick-<?php print $style;?>"></div>
<div class="custom_module_tpl_blocks set-<?php print $set;?> relative">
<?php if($set<>2 && $set<>'services'){?>
    <div class="tit_wrapper_j">
      
      <?php 
      $parts = explode(' ', $mtitle);
      foreach($parts as $wordnum=>$word){
        print '<h2 class="btitle btitle-'.$wordnum.'">'.$parts[$wordnum].'</h2>';
      } ?>
    </div>
<?php }elseif($set=='services'){?>
      <h2 class="btitle"><?php print $mtitle;?></h2>
<?php }?>
    
<ul class="wrapper_blocks" rel="<?php print $style;?>" set="set-<?php print $set;?>">
<?php
$cntP=0;
if(isset($mtree[$mid]['#below']) && count($mtree[$mid]['#below']) > 0){
foreach( $mtree[$mid]['#below'] as $key=>$v ){
$cntP++;
 if (   ( $set==1 && $cntP <=4) ||  ( $set==2 && $cntP >4 ) || $set=='services')  {
  if(isset($v['#title']) && $v['#title']<>''){
    $hasSubs='';
    if(sizeof($v['#below']) > 0 ) {
      $subs[$key] = $v['#below']; $hasSubs=" hasSubs";
    }
    else $hasSubs=" doesNOThaveSub";
    ?>
 <li class="col-lg-3 col-xs-6 col-sm-3 triger_drop_cat mid-<?php print $key.' '.strtolower(drupal_clean_css_identifier( $v['#title'] )); print $hasSubs;?>" rel="blk-<?php print $key ;?>">
     
     <?php if($hasSubs<>' doesNOThaveSub'){?>
     <a class="bg"></a>
      <div class="title"><?php print $v['#title'] ;?></div>
     <?php }else{ ?>
      <a class="bg" href="<?php print url($v['#href'])?>"></a>
      <?php print l($v['#title'] , $v['#href'] , array('attributes'=>array('id'=>'' , 'class'=>array('title' ,'doesNOThaveSubID') )));?>
     <?php }?>    
 </li>
<?php }
}
}}?>
</ul>

<div class="clearfix"></div> 
</div>
</div>
<div class="plants_detail_box <?php print $style;?> set-<?php print $set;?>">
    <div class="det_top"></div>
      <div class="det_middle">
        <?php 
        foreach( $subs as $k=>$vv ){
          $below=ca($vv);
          $cntBLW=count($below);
          ?>
        <div class="blk blk-<?php print $k;?>">
        <?php $n=0;  foreach( $below as $c=>$b ){?>
<?php if($cntBLW>1 && sizeof($b['#below'])>0){?>            
<div class="<?php print (($n%2)==1) ? 'right-j fr' : 'left-j fl';?>" mod="<?php print $n%2;?>">
<?php }?>
            <?php if(sizeof($b['#below'])>0) $paretnClass='hasChildren fl';?>
                  <div class="title2 <?php print $paretnClass.' c-'.$c; $paretnClass='';?>">
                    <?php print $b['#href'] <> '<nolink>' ? l($b['#title'],$b['#href']) : $b['#title'] ;?>
                  </div>
            
             <?php if(sizeof($b['#below'])>0){?>
                  <div class="fl childs">
                      <?php 
                      $ar=ca($b['#below']);
                      foreach($ar as $kkk=>$vvv){?>
                         <div class="smallest"><?php print l($vvv['#title'],$vvv['#href'])?></div>
                      <?php }?>
                  </div>
                <?php }?>
                  
<?php if($cntBLW>1 && sizeof($b['#below'])>0){?>            
</div>
            
            
            
<?php if(($n%2)==1) print '<div class="clearfix"></div>'; 

}?>
            
        <?php 
                      $n++;
                      
                      }?>
            
            
            <div class="clearfix"></div>
        </div>
        <?php }?>
    </div>
    <div class="det_bottom"></div>
</div>

</div>
<?php }else{?>
<div class=""> 

    
    
<div id="wrapper-<?php print $style;?>" class="relative">
<div class="absolute white-layer-trick white-layer-trick-<?php print $style;?>"></div>
<div class="custom_module_tpl_blocks set-<?php print $set;?>">
    <h2 class="btitle"><?php print $mtitle?></h2>
<ul class="wrapper_blocks" rel="<?php print $style;?>" set="set-<?php print $set;?>">
<?php
$cntP=0;
if(isset($mtree[$mid]['#below']) && count($mtree[$mid]['#below']) > 0){
foreach( $mtree[$mid]['#below'] as $key=>$v ){
$cntP++;
 if (isset($v['#title']) && $v['#title']<>'' ){
 
    $hasSubs='';
    if(sizeof($v['#below']) > 0 ) {
      $subs[$key] = $v['#below']; $hasSubs=" hasSubs";
    }
    else $hasSubs=" doesNOThaveSub";
    ?>
    <li myrow="<?php print $cntP?>" class="col-lg-3 col-xs-6 col-sm-3 triger_drop_cat mid-<?php print $key.' '.strtolower(drupal_clean_css_identifier( $v['#title'] )); print $hasSubs;?>" rel="blk-<?php print $key ;?>">
       <?php if($hasSubs<>' doesNOThaveSub'){?>
     <a class="bg"></a>
      <div class="title"><?php print $v['#title'] ;?></div>
     <?php }else{ ?>
      <a class="bg" href="<?php print url($v['#href'])?>"></a>
      <?php print l($v['#title'] , $v['#href'] , array('attributes'=>array('id'=>'' , 'class'=>array('title' ,'doesNOThaveSubID') )));?>
     <?php }?>
     
 </li>
<?php 
if ( ($cntP%2) ==0 ){

 ?>
 <li class="split_li col-xs-12"><div class="plants_detail_box <?php print $style; print ' cntp-'.$cntP;?> set-<?php print $set;?>">
         <div class="det_top"></div>
         <div class="det_middle">
        <?php 
        foreach( $subs as $k=>$vv ){
          $below=ca($vv);
          $cntBLW=count($below);
          ?>
             <div class="blk blk-<?php print $k;?>">
        <?php $n=0;  foreach( $below as $c=>$b ){?>
<?php if($cntBLW>1 && sizeof($b['#below'])>0){?>            
                 <div class="<?php print (($n%2)==1) ? 'right-j fr' : 'left-j fl';?>">
<?php }?>
            <?php if(sizeof($b['#below'])>0) $paretnClass='hasChildren fl';?>
                     <div class="title2 <?php print $paretnClass.' c-'.$c; $paretnClass='';?>">
                    <?php print $b['#href'] <> '<nolink>' ? l($b['#title'],$b['#href']) : $b['#title'] ;?>
                     </div>
                    
             <?php if(sizeof($b['#below'])>0){?>
                     <div class="fl childs">
                      <?php 
                      $ar=ca($b['#below']);
                      foreach($ar as $kkk=>$vvv){?>
                         <div class="smallest"><?php print l($vvv['#title'],$vvv['#href'])?></div>
                      <?php }?>
                     </div>
                <?php }?>
                     
<?php if($cntBLW>1 && sizeof($b['#below'])>0){?>            
                 </div>
<?php }?>
  
        <?php 
                      $n++;
                        
                      }?>
                        
                        
                 <div class="clearfix"></div>
             </div>
        <?php }$subs=array();?>
         </div>
         <div class="det_bottom"></div>
     </div>
 </li>
 
 
 <?php } }
}}?>
</ul>

<div class="clearfix"></div> 
</div>
</div>

    
    
</div>
<?php }?>