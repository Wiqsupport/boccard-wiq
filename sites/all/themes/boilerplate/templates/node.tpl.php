<?php 
global $language;
$lang = $language->language;
$node_ref = $node->field_banner_reference['und'][0]['node'];
$node_files = $node->field_files['und'];

$path = 'node/'.$node->nid;
$parent = menu_link_get_preferred($path);
$p=menu_link_load($parent['plid']);
$parameters = array(
    'active_trail' => array($parent['mlid']),
    'only_active_trail' => FALSE,
    'min_depth' => $parent['depth'],
    'max_depth' => $parent['depth'],
    'conditions' => array('plid' => $parent['plid']),
  );

$children = menu_build_tree($parent['menu_name'], $parameters);
foreach($children as $v){
  $dropentries[] = l($v['link']['link_title'],$v['link']['link_path'] , array('query'=>array('o'=>1)));
}
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; print " node_ref_nid_".$node_ref->nid;?>">
  <div class="node-inner">

      <div class="my_node_title"><?php print $node->title;?></div>
<?php print $node->body['und'][0]['value'];?>      

  </div>
</div>