<div class="block_press_tpl_page">
    
<div class="banner-locations">
<div class="banner-locations-inner">
<h1 class="locations-ptitle"><?php print t('News & Press')?></h1>
</div>
</div>
<div class="news_press_links">
    <div class="wrap wrap_press fl"><?php print l( t('NEWS') , 'news');?></div>
    <div class="wrap wrap_news fl"><?php print l( t('PRESS') , 'press');?></div>
    <div class="clearfix"></div>
</div>
<div class="padding_2">
<?php 
$cnt=count($rows);
foreach ($rows as $row_count => $row):
foreach ($row as $field => $content): 
  switch($field){
    case 'body' : $body = $content; break;
    case 'nid' : $nid = $content; break;
    case 'title' : $title_f = $content; break;
    case 'field_brief' : $field_brief = $content; break;
    case 'fid' : $fid = $content; break;
  }
?>
<?php endforeach; ?>

<div class="bl <?php print ($row_count%2)==1 ? 'fr right_bl':'fl left_bl'; print 'pressnid-'.$nid?>">
    <div class="title"><?php print $title_f?></div>
    <div class="body"><?php print $body?></div>
    <div class="brief"><?php print $field_brief?></div>
    <?php if($fid<>''){?>
    <div class="pdf">
      <?php print l(t('Download the article'), 'download/file/fid/' . $fid); ?>
          <a class="bg" href="<?php print url('download/file/fid/' . $fid); ?>"></a>
          <div class="clearfix"></div>
    </div>
    <?php }?>
</div>        
    
<?php 
if(($row_count%2)==1 || $row_count==($cnt-1)) print '<div class="clearfix"></div>';

endforeach; ?>
</div>
</div>
