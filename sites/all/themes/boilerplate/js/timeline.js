(function($){
	
  $(document).ready(function(){
		
		var wW = $(window).width();
		if( wW < 768 ) adjustHistorySlider(wW);
		
		$(window).on('resize', function () {
			wW = $(window).width();
			if( wW < 768 ) adjustHistorySlider(wW);
		}).trigger('resize');

		if( wW < 768 ) {
			$('#dock a').click(function(){
				var myIndex = $(this).attr("data-index");
				var halfScreen = wW/2;
				var myPosition = $(this).position();
				var leftPosition = eval(myPosition.left - halfScreen);
				
				$('#dock a').find('div.detailBox').hide();
				$(this).find('div.detailBox').show();
					
				if( $(this).find('div.arrow-slider').length>0 ){
					$('#dock a').find('div.arrow-slider').hide();
					$(this).find('div.arrow-slider').show();
					$("#dock").animate({scrollLeft: leftPosition},500,function(){
						//animate main slider
						animateNextSlide(myIndex);
						var slider = $('.history-slider');
						slider[0].slick.slickGoTo(parseInt(myIndex));
					});
				}
			}); 
		}else{
			$(function(){
				// Dock initialize
				$('#dock').Fisheye({
						maxWidth: 40,
						items: 'a',
						itemsText: 'span',
						container: '.dock-container',
						itemWidth: 8,
						proximity: 30,
						alignment : 'left',
						halign : 'center',
						valign : 'top'
				});
			});   
			$('#dock a').click(function(){
				var myIndex = $(this).attr("data-index");
				animateNextSlide(myIndex);
				$('#dock a').find('div.arrow-slider').hide();
				$(this).find('div.arrow-slider').show();
				var slider = $('.history-slider');
				slider[0].slick.slickGoTo(parseInt(myIndex));
			});
		}
		
		$('.history-slider').slick({
			dots: false,
			infinite: false,
			speed: 1000,
			slidesToShow: 1,
			slidesToScroll: 1,
			initialSlide: 0,
			variableWidth: true,
			dots: false,
			centerMode: true,
			responsive: [
				{
				  breakpoint: 768,
				  settings: {
					arrows: true,
					centerMode: false,
					slidesToShow: 1
				  }
				}
		  ]
		});
		
		// On before slide change
		$('.history-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			var wW = $(window).width();
			if( wW < 768 ){
				$("#dock a.dock-item[data-index='"+nextSlide+"']").trigger("click");
			}
			currentSlide++;// = (currentSlide==0)?1:currentSlide+1;
			nextSlide++;
			hideSlide(currentSlide);
			animateNextSlide(nextSlide);
		});
		
		// On after slide change
		$('.history-slider').on('afterChange', function(event, slick, currentSlide){
			currentSlide++;// = currentSlide+1;
			showSlide(currentSlide);
		});
		
		disableButton();
		
		setTimeout(function(){
			$(".slide-1 .movie-clip-1").animate({width: '500px'}, "5000", "linear",function(){
				$(".slide-1 .movie-clip-2").animate({height: '70px'}, "5000", "linear",function(){
					showSlide(1);
				});
			});	
		},200);
      
	  function adjustHistorySlider(wW){
	  	var containerWidth = parseInt($("#dock .dock-container").attr("data-width"));
		var marginLeft = parseInt(wW/2)-7;
		var allWidth = eval(containerWidth + marginLeft);
		
		$("#dock .dock-container").width( allWidth );
		$("#dock .dock-container a.dock-item").eq(0).css({'margin-left': marginLeft+"px" });
		
		$(".history-slider .single-slide").width(wW);
		
		$(".single-slide .box-container").each(function(){
			var $this = $(this);
			if($this.find('.box-logos').length>0){
				$this.find('.box-data').css({'padding-bottom':'100px'});
			}
		});
		
	  }
	  
	  function disableButton(){
	  		return true;
			$("button.slick-prev").addClass("slick-disabled");
			$("button.slick-prev").attr("aria-disabled","true");
	  }
	  
      function animateNextSlide(mySlide){
		var $allClip = $(".slide-"+mySlide).find(".movie-clip");
		var j=0;
		
		if(mySlide==1){
			disableButton();
		}
		
		$allClip.each(function(i, elem) {
			var $this = $(elem);
			var thisMove = $this.attr("data-move");
			var thisLength = $this.attr("data-length");
			var myClass = $this.attr("class").replace("movie-clip ","");
			i++;
			var timeOut = j*600;
			
			if(thisMove=="width"){
				var data = {width: thisLength};
			}else if(thisMove=="height"){
				var data = {height: thisLength+"px"};
			}
			
			setTimeout(function(){
				$(".slide-"+mySlide+" ."+myClass).animate(data, 600, "linear",function(){});
			},timeOut);
			j++;
		});
	}
	
	function showSlide(mySlide){
		$(".slide-"+mySlide).find(".box-container").fadeIn();
		if(mySlide==1){
			disableButton();
		}
	}
	
	function hideSlide(mySlide){
		$(".slide-"+mySlide).find(".box-container").fadeOut();
		if(mySlide==1){
			disableButton();
		}
	}
      
      
    });
})(jQuery);