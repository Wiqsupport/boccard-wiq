(function($) {
    $(function() {
        
        
          $("#slider_logos li").each(function(){
           var $img = $(this).children();
           var h = $img.outerHeight() ;
           var marginTop = (90 - h) /2;
           
          // $img.css( 'margin' , marginTop+"px auto 0 auto" );
           
        });
        
        
        var jcarousel = $('.jcarousel');

        jcarousel.on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 600) {
                    width = width / 4;
                } else if (width >= 350) {
                    width = width / 2;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                //carousel.jcarousel('items').children().attr('width','');
                //carousel.jcarousel('items').children().attr('height','');
                
            })
            .jcarousel({
                wrap: 'circular',
                scroll: 1,
                visible: 4,
                animation: 1500,
                auto: 1,
                itemFallbackDimension: 75
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
            
    });
})(jQuery);
