(function($){
  $(document).ready(function(){


var country="all";
var concat='';
var loaded=1;

function initialize(zoom) {
    concat='';
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
        center: new google.maps.LatLng(0, 0),
        zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var infowindow = new google.maps.InfoWindow();
    
    
    
     var url= Drupal.settings.basePath + 'contacts-source/' + country;
    
    $.ajax({
                url: url ,
                dataType: 'json',
                success: function(data) {
                    var is_last ='';
                    var  new_boundary = new google.maps.LatLngBounds();
                   if(data.length > 0 ){
                    $.each(data, function(i){
                        if( data[i].field_lat.length > 2  ){
                            
                        if(data.length ==(i+1)) is_last="last";
                        
                    concat +=  '<div class="html_contacts_popup_listing '+data[i].nid+' '+is_last+'">';
                        concat += '<div class="inner">';
                        concat += '<div class="fl left-one">';
                           if(data[i].title1 !='') concat += '<div class="popc_title">'+data[i].title+'</div>';
                           if(data[i].field_adresse !='') concat += '<div class="popc_field_adresse">'+data[i].field_adresse+'</div>';
                           if(data[i].field_adresse_postale !='') concat += '<div class="popc_field_adresse_postale">'+data[i].field_adresse_postale+'</div>';
                           if(data[i].field_country !='') concat += '<div class="popc_field_country">'+data[i].field_country+'</div>';
                           if(data[i].field_extra !='') concat += '<div class="popc_siege_social">'+data[i].field_extra+'</div>';
                           
                        concat += '</div>';
                    concat += '<div class="fr right-one">';

                   if(data[i].field_tel !='') concat += '<div class="picto popc_field_tel">'+data[i].field_tel+'</div>';
                   if(data[i].field_fax !='') concat += '<div class="picto popc_field_fax">'+data[i].field_fax+'</div>';
                   if(data[i].field_email !='') concat += '<div class="picto popc_field_email"><a href="mailto:'+data[i].field_email+'">'+data[i].field_email+'</a></div>';
                   
                        concat += '</div>';
                        concat += '<div class="clearfix"></div>';
                    concat += '</div>';
                    concat += '</div>';
                            
                            
                            
                                var coords = new google.maps.LatLng( data[i].field_lat , data[i].field_long );
                                var marker = new google.maps.Marker({
                                    position: coords,
                                    map: map,
                                    icon: Drupal.settings.pathToTheme+ '/images/contacts-picto.png',
                                });
                                new_boundary.extend(marker.position);
                                google.maps.event.addListener(marker, 'click',
                                    (function(data, marker, k) {
            return function(  ) {
                    var concat ="";
                    concat +=  '<div class="html_contacts_popup '+data[i].nid+'">';
                   if(data[i].title !='') concat += '<div class="popc_title">'+data[i].title+'</div>';
                   if(data[i].field_adresse !='') concat += '<div class="popc_field_adresse">'+data[i].field_adresse+'</div>';
                   if(data[i].field_adresse_postale !='') concat += '<div class="popc_field_adresse_postale">'+data[i].field_adresse_postale+'</div>';
                   if(data[i].field_country !='') concat += '<div class="popc_field_country">'+data[i].field_country+'</div>';
                   if(data[i].field_extra !='') concat += '<div class="popc_siege_social">'+data[i].field_extra+'</div>';
                   
                   if(data[i].field_tel !='') concat += '<div class="picto popc_field_tel">'+data[i].field_tel+'</div>';
                   if(data[i].field_fax !='') concat += '<div class="picto popc_field_fax">'+data[i].field_fax+'</div>';
                   if(data[i].field_email !='') concat += '<div class="picto popc_field_email"><a href="mailto:'+data[i].field_email+'">'+data[i].field_email+'</a></div>';
                    concat += '</div>';

                infowindow.setContent(concat);
                infowindow.open(map, marker);
            }
                                 })(data, marker, i));
                            }
                        });//each                              
                                    map.fitBounds(new_boundary);  
                                        var listener = google.maps.event.addListener(map, "idle", function () {
                                         if(zoom=="zoom_it")  map.setZoom(6);
                                            google.maps.event.removeListener(listener);
                                        });
                                        
                        if( loaded !=1 ) $('#wrapper-contact-list').html(concat);
                        loaded=2;
                        
                    }// if data
                }//success
            });
  

}

google.maps.event.addDomListener(window, 'load', initialize);

$("#country_input").change(function(){
    country = $(this).val();
   initialize('zoom_it');
   
});

    });
})(jQuery);
