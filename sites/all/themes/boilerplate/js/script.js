(function ($) {
    $(document).ready(function () {

    	
    	
    	/**
    	 * NEWS JS
    	 */
    	$('#filter_xs').click(function() {
    		if($('.search_bloc').is(':visible'))
    		{
    			$('.search_bloc').slideUp('slow');
    		}
    		else
    		{
    			$('.search_bloc').slideDown('slow');
    		}
    	});
    	
    	
    	

$(".det_middle a").click(function(){
   var href = $(this).attr('href');
   var nid = $(this).attr('nid');
   // console.log(nid);
   var target = ''; 
    
    if(nid==386 || nid== 801 || nid==904 || nid==1025) target='UpStream';
    if(nid==762 || nid==807  || nid==914 || nid==1026) target='DownStream';
    if(nid==549 || nid==806  || nid==916 || nid==1029) target='Systemes Fluides';
    if(nid==551 || nid==808  || nid==917 || nid==1031) target='Modifications';
    if(nid==552 || nid==809  || nid==918 || nid==1032) target='Maintenance';
    if(nid==554 || nid==831  || nid==930 || nid==1035) target='Reseaux de tuyauteries';
    if(nid==563 || nid==832  || nid==931 || nid==1036) target='Balance Plant';
    if(nid==564 || nid==836  || nid==933 || nid==1040) target='Usines cles main';
    if(nid==566 || nid==833  || nid==932 || nid==1038) target='Solutions cles main & EPC';
    if(nid==567 || nid==834  || nid==934 || nid==1042) target='Efficacite energetique';
    if(nid==565 || nid==835  || nid==935 || nid==1043) target='Optimisation & renovation';
    if(nid==553 || nid==838  || nid==936 || nid==1045) target='Siderurgie';
    if(nid==763 || nid==839  || nid==937 || nid==1046) target='Automobile';
    if(nid==764 || nid==840  || nid==938 || nid==1047) target='Papier';
    if(nid==878 || nid==1093  || nid==939 || nid==1048) target='Javel';
    if(nid==560 || nid==810  || nid==919 || nid==1050) target='Brasserie';
    if(nid==756 || nid==812  || nid==920 || nid==1051) target='Extrait de malt';
    if(nid==757 || nid==813  || nid==921 || nid==1052) target='Distillation';
    if(nid==561 || nid==819  || nid==922 || nid==1055) target='Produits laitiers';
    if(nid==576 || nid==818  || nid==923 || nid==1056) target='Crèmes glacées & Desserts';
    if(nid==569 || nid==820  || nid==924 || nid==1057) target='Alimentation infantile';
    if(nid==570 || nid==821  || nid==925 || nid==1058) target='Sauces & Soupes';
    if(nid==571 || nid==822  || nid==926 || nid==1059) target='Plats préparés';
    if(nid==572 || nid==823  || nid==927 || nid==1060) target='Bio-Procédés';
    if(nid==574 || nid==824  || nid==928 || nid==1068) target='Boissons non alcoolisées';
    if(nid==575 || nid==825  || nid==929 || nid==1069) target='Vins et Spriritueux';
    if(nid==559 || nid==845  || nid==940 || nid==1070) target='Cosmetique';
    if(nid==758 || nid==846  || nid==941 || nid==1072) target='Hygiene';
    if(nid==557 || nid==850  || nid==942 || nid==1075) target='Sirops, suspensions et crèmes';
    if(nid==558 || nid==851  || nid==943 || nid==1076) target='Injectables & produits parenteraux';
    if(nid==556 || nid==852  || nid==944 || nid==1078) target='Fractionnement sanguin';
    if(nid==555 || nid==853  || nid==945 || nid==1079) target='Biotechnologies';
    if(nid==790 || nid==854  || nid==946 || nid==1080) target='Bio-Procédés' ;
    
    
    ga('send', 'event', 'HOME-Menu1', 'Click', target);
    
});

$(".explore_link").click(function(){
   ga('send', 'event', 'HOME-Presence', 'Click', 'Lien Business units');
});


$(".explore_boccard_loc .bg_arrow").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Fleche');
});

$(".map_click_ga").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Presence dans le monde');
});

$(".row-wrap-news-0 a").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Actu1');
});
$(".row-wrap-news-1 a").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Actu2');
});
$(".row-wrap-news-2 a").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Actu3');
});

$("#block-custom-front_talented a").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Bandeau');
});


$("#home_contactBtn a").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Contact' );
});

$(".social-btns ul li.fb").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Facebook' );
});
$(".social-btns ul li.in").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Linkedin' );
});
$(".social-btns ul li.tw").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Twitter' );
});
$(".social-btns ul li.ut").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Youtube' );
});

$(".private_access a").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Acces prive' );
});

$(".main_btn_cont a").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Contact US' );
});

$("#custom-contact-form .form-submit").click(function(){
    ga('send', 'event', 'HOME-Presence', 'Click', 'Envoyer' );
});
       

        if ( Drupal.settings.custom.lang !== undefined ){
            var my_lang = Drupal.settings.custom.lang;
            var my_lang_srch = my_lang != '' ? my_lang+'/' : '';
            if(my_lang=='zh-hans') my_lang_srch = 'cn/'
        }

        var wW = '';
        var hH = '';
        var $allVideos = $("iframe[src^='//www.youtube.com']"),
                $fluidEl = $("body");
        $allVideos.each(function () {
            $(this).data('aspectRatio', this.height / this.width).removeAttr('height').removeAttr('width');
        });

        $(window).on('resize', function () {
            wW = $(window).width();
            hH = $(window).height();
            // alert(wW + ' ' + hH);
            var newWidth = $fluidEl.width();
            $allVideos.each(function () {
                var $el = $(this);
                $el.width(newWidth).height(newWidth * $el.data('aspectRatio'));
            });
        }).trigger('resize');

        var wWs ='';
        if(wW >= 1024) wWs = 1024;
        else wWs = wW;
        
        if( wW < 768 ){
            
            var ddW = $('.mobile-dropdown_title_brief .dropdown .txt').outerWidth() + 35;
            $('.mobile-dropdown_title_brief .dropdown').css({
                'margin-left': ( (wW - ddW) / 2 ) + 1
            });
            
            var testH = $("#js_testi_bann").outerHeight();
            var btn_this = $(".applybtn");
            var mLeft = parseInt( (wW - btn_this.outerWidth()) /2 );
            btn_this.css({
                'left' : mLeft ,
                'top' : testH - 23
            });
            
            
            var btn_this = $(".talelink");
            var mLeft = parseInt( (wW - btn_this.outerWidth()) /2 );
            btn_this.css({
                'top' : testH + 43,
                'left' : mLeft ,
            });
            
            var btn_this = $(".disk_open_close");
            var mLeft = parseInt( (wW - btn_this.outerWidth()) / 2 );
            btn_this.css({
                'left' : mLeft ,
                'margin-left' : 0 ,
                'top' : testH + 125
                });
            var btn_this = $(".rounded");
            var mLeft = parseInt( (wW - btn_this.outerWidth()) / 2 );
            btn_this.css({
                'left' : mLeft
                });
            
            var testH = $(".node-page .banner").outerHeight();
            var btn_this = $(".node-page .disk_open_close");
            btn_this.css({
                'top' : testH-110
            });
            
        }
        
        

        //alert(wW + ' X ' + hH);
        var num_slides = $('.s1 .swiper-slide').size();
        var visible = 5;
        var speed = 1500;
        if (wW < 768) {
            visible = 1;
            speed = 800;
        }else{
            $('.s1 .swiper-wrapper').css({
                'margin':'0 auto',
                'width': (num_slides * 220)+'px'
            });
            if( num_slides < 6 ) {
                $(".swiper-button-ss-prev,.swiper-button-ss-next").hide();
            }
        }
        
if ( $('.s1').length ) {
    

       /* else{
            $('.s1 .swiper-wrapper').css({
                'margin':'0 auto',
                'width': (num_slides * 220)+'px'
            });
        }*/
    
        var swiper1 = new Swiper('.s1', {
            pagination: '.swiper-pagination',
            slidesPerView: visible,
            paginationClickable: true,
            spaceBetween: 0,
            autoplay: 5000,
            speed: speed
            });
        var swiperControl = $('.s1')[0].swiper;
            $(".swiper-button-ss-prev").click(function () {
                swiperControl.slidePrev();
            });
            $(".swiper-button-ss-next").click(function () {
                swiperControl.slideNext();
            });
        
}
 
        if ($('.swiper-container').length) {
            var swiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                slidesPerView: 1,
                paginationClickable: true,
                spaceBetween: 0,
                autoplay: 5000,
                speed: 800,
                loop: true
            });

            var mySwiper = $('.swiper-container')[0].swiper;
            $(".swiper-button-prev").click(function () {
                mySwiper.slidePrev();
            });
            $(".swiper-button-next").click(function () {
                mySwiper.slideNext();
            });
        }
        if ($('.swiper-container-mob').length) {

            var swipermob = new Swiper('.swiper-container-mob', {
                pagination: '.swiper-pagination',
                slidesPerView: 1,
                paginationClickable: true,
                spaceBetween: 0,
                autoplay: 5000,
                speed: 800,
                loop: true
            });

            var mySwiperMob = $('.swiper-container-mob')[0].swiper;
            $(".swiper-news-mob-button-prev").click(function (e) {
                mySwiperMob.slidePrev();
            });
            $(".swiper-news-mob-button-next").click(function (e) {
                mySwiperMob.slideNext();
            });
        }
        
        
        
        if (wW < 1024) {

            $("#block-custom-front_services li.triger_drop_cat").click(function () {
                var block = $(this).attr('rel');
                var parent = "#block-custom-front_services";
                var ind = $(this).index();
                $(".plants_detail_box.set-services").show();
                
                $(parent+' .det_middle .blk').hide();
                $(parent+' .'+block).show();
                $(parent+' #wrapper-services .custom_module_tpl_blocks').css('padding-bottom' , '32px');
                if(ind >1) $(parent+' #wrapper-services .custom_module_tpl_blocks').css('padding-bottom' , '0');
                
                
            });
            
            $("#block-custom-front_services_mobile li.triger_drop_cat").click(function () {
                
                if( $(this).find('a.bg').attr('href') === undefined  ) 
                {
                
                
                
                var ind = $(this).index();
                var block_id = $(this).attr('rel');
                var myrow = $(this).attr('myrow');
                var set = $(this).closest('.wrapper_blocks').attr('set');
           
                    var myV = 0;
                    if (myrow <= 2){
                        myV = 2;
                    }
                    if (myrow == 3 || myrow == 4){
                        myV = 4;
                    }
                $("#block-custom-front_services_mobile .plants_detail_box").hide();
                $("#block-custom-front_services_mobile .blk").hide();
                
                $("#block-custom-front_services_mobile .plants_detail_box.cntp-"+myV+"."+set).closest('.split_li').show();
                $("#block-custom-front_services_mobile .plants_detail_box.cntp-"+myV+"."+set).show();
                //$("#block-custom-front_services_mobile .split_li").height(0);
                
                    
               
                //$("#block-custom-front_services_mobile .split_li").css('z-index','0');
                //$("#block-custom-front_services_mobile .split_li-"+myV+"."+set).css('z-index','999');
                        
                $("."+block_id).show();
                //ar outer_blk_H = $("."+block_id).closest('li').find('.plants_detail_box').outerHeight();
                //$("#block-custom-front_services_mobile .split_li-"+myV).height(outer_blk_H);
    
                //if(myV==4) 
                
                    $("#block-custom-front_services_mobile .custom_module_tpl_blocks").css('padding-bottom', '32px');
                 if(ind > 1)   $("#block-custom-front_services_mobile .custom_module_tpl_blocks").css('padding-bottom', '0');
                
                $('.triger_drop_cat').removeClass('active');
                
                if ( !$(this).hasClass('active') ) {
                    $(this).addClass('active');
                    $('.' + block_id).show();
                }
                else {
                    $(this).removeClass('active');
                }
            }
            else{
                return;
            }
            });
            
            
            $(".dddd .wrapper_blocks li.triger_drop_cat").click(function () {
                var ind = $(this).index();
                var block_id = $(this).attr('rel');
                var myrow = $(this).attr('myrow');
                var set = $(this).closest('.wrapper_blocks').attr('set');
           
                    var myV = 0;
                    if (myrow <= 2){
                        myV = 2;
                    }
                    if (myrow == 3 || myrow == 4){
                        myV = 4;
                    }
                
                $("#block-custom-home_page_menu_grid .split_li").height(0);
                $("#block-custom-home_page_menu_grid .plants_detail_box").hide();
                $("#block-custom-home_page_menu_grid .blk").hide();
                    
                $("#block-custom-home_page_menu_grid .plants_detail_box.cntp-"+myV+"."+set).show();
               
                $("#block-custom-home_page_menu_grid .split_li").css('z-index','0');
                $("#block-custom-home_page_menu_grid .split_li-"+myV+"."+set).css('z-index','909');
                        
                $("."+block_id).show();
                var outer_blk_H = $("."+block_id).closest('li').find('.plants_detail_box').outerHeight();
              
                if( wW > 767 ){
                    var css_options={
                        'z-index':'909',
                        'height':outer_blk_H ,
                        'width': '200%' ,
                        'position' : 'relative',
                        'left' : '-512px',
                        'margin-left': 0
                    };
                    if(set=='set-1') css_options['left'] = 0;
                    
                    $("#block-custom-home_page_menu_grid .split_li-"+myV).height(outer_blk_H);
                    $("#block-custom-home_page_menu_grid .split_li-"+myV+"."+set).css(css_options);
                    
                }else{
                    $("#block-custom-home_page_menu_grid .split_li-"+myV+"."+set).height(outer_blk_H);
                }
    
                if(myV==4) $("#block-custom-home_page_menu_grid #wrapper-plants .custom_module_tpl_blocks").css('margin-bottom', '0');
                
                $('.triger_drop_cat').removeClass('active');
                
                if ( !$(this).hasClass('active') ) {
                    $(this).addClass('active');
                    $('.' + block_id).show();
                }
                else {
                    $(this).removeClass('active');
                }
                
            });
        }

        else {
            
            $("#block-custom-front_services .wrapper_blocks li.triger_drop_cat").mouseover(function () {

                var ind = $(this).index();
                var block_id = $(this).attr('rel');
                var myrow = $(this).attr('myrow');
                var style = $(this).closest('.wrapper_blocks').attr('rel');
                var set = $(this).closest('.wrapper_blocks').attr('set');
                var $childBlk = $("#block-custom-front_services  .plants_detail_box." + style + "." + set);


                if (!$(this).hasClass('hasSubs')) {
                    $("#block-custom-front_services .plants_detail_box").hide();
                    $('#block-custom-front_services .wrapper_blocks li').removeClass('active');
                    return;
                }
                $("#block-custom-front_services .plants_detail_box").hide();
                if (set != 'set-mobile')
                    $childBlk.show();
                else {
                    var myV = 0;
                    if (myrow <= 2)
                        myV = 2
                    if (myrow == 3 || myrow == 4)
                        myV = 4
                    if (myrow == 5 || myrow == 6)
                        myV = 6
                    if (myrow == 7 || myrow == 8)
                        myV = 8
                    var $childBlk = $("#block-custom-front_services .plants_detail_box." + style + "." + set + ".cntp-" + myV);
                    $childBlk.show();
                }

                $('#block-custom-front_services .wrapper_blocks li').removeClass('active');
                $('#block-custom-front_services .blk').hide();
                
                if (!$(this).hasClass('active')) {
                    $(this).addClass('active');
                    $('.' + block_id).show();
                }
                else {
                    $(this).removeClass('active');
                }
                
            });
            
            
            $("#block-custom-front_services").mouseleave(function () {
                $("#block-custom-front_services #wrapper-plants .custom_module_tpl_blocks").css('padding-bottom', '38px');
                $('#block-custom-front_services .wrapper_blocks li').removeClass('active');
                $("#block-custom-front_services .plants_detail_box").hide();
            });
            
            
            
            /**/
            
            $(".dddd .wrapper_blocks li.triger_drop_cat").mouseover(function () {
                var ind = $(this).index();
                var block_id = $(this).attr('rel');
                var myrow = $(this).attr('myrow');
                var set = $(this).closest('.wrapper_blocks').attr('set');
           
                    var myV = 0;
                    if (myrow <= 2){
                        myV = 2;
                    }
                    if (myrow == 3 || myrow == 4){
                        myV = 4;
                    }
                
                $("#block-custom-home_page_menu_grid .split_li").height(0);
                $("#block-custom-home_page_menu_grid .plants_detail_box").hide();
                $("#block-custom-home_page_menu_grid .blk").hide();
                    
                $("#block-custom-home_page_menu_grid .plants_detail_box.cntp-"+myV+"."+set).show();
               
                $("#block-custom-home_page_menu_grid .split_li").css('z-index','0');
                $("#block-custom-home_page_menu_grid .split_li-"+myV+"."+set).css('z-index','999');
                        
                $("."+block_id).show();
                var outer_blk_H = $("."+block_id).closest('li').find('.plants_detail_box').outerHeight();
                $("#block-custom-home_page_menu_grid .split_li-"+myV).height(outer_blk_H);
    
                if(myV==4) $("#block-custom-home_page_menu_grid #wrapper-plants .custom_module_tpl_blocks").css('margin-bottom', '0');
                
                $('.triger_drop_cat').removeClass('active');
                
                if ( !$(this).hasClass('active') ) {
                    $(this).addClass('active');
                    $('.' + block_id).show();
                }
                else {
                    $(this).removeClass('active');
                }
                
            });
            
            $("#block-custom-home_page_menu_grid").mouseleave(function () {
                $("#block-custom-home_page_menu_grid .split_li").height(0);
                $("#block-custom-home_page_menu_grid .plants_detail_box").hide();
                $("#block-custom-home_page_menu_grid .blk").hide();
                $("#block-custom-home_page_menu_grid #wrapper-plants .custom_module_tpl_blocks").css('margin-bottom', '38px');
                $('#block-custom-home_page_menu_grid .wrapper_blocks li').removeClass('active');
                $("#block-custom-home_page_menu_grid .plants_detail_box").hide();
            });
            
            
            
            /**/
            
            
        }

        $(".doesNOThaveSub, .doesNOThaveSub a").mouseover(function () {
            $(this).addClass("active");
        });
        $(".doesNOThaveSub, .doesNOThaveSub a").mouseout(function () {
          //  $(this).removeClass("active");
        });


        /**** NEWS #block-views-news-block **/
        if (wW > 768) {
            $(".row-wrap-news").mouseover(function () {
                $(this).find(".show-hide-hover").slideDown("fast");
            });
            $(".row-wrap-news").mouseleave(function () {
                $(this).find(".show-hide-hover").slideUp("fast");
            });
        }

        if (wW < 1024)
            $('#sidebar').addClass("removed").hide();

        setTimeout(function () {
            $('#sidebar').addClass("removed").hide();
        }, 20000);

        if ($("#sidebar").length) {
            var offset = $("#sidebar").offset();
            var sidebarH = $("#sidebar").outerHeight();
            //var topPadding = ( $(window).height() - sidebarH)/2 ;
            var topPadding = 720;

            setTimeout(function () {
                $("#sidebar").stop().animate({marginTop: topPadding}, 800)
            }, 1000);

            $(window).scroll(function () {
                if (!$("#sidebar").hasClass('removed')) {
                    if ($(window).scrollTop() > offset.top) {
                        $("#sidebar").stop().animate({
                            marginTop: ($(window).scrollTop() - offset.top + topPadding) - 220
                        });
                    } else {
                        $("#sidebar").stop().animate({
                            marginTop: 0
                        });
                    }
                    ;
                }
            });
        }
        $(".help_close").click(function () {
            ga('send', 'event', 'Aide', 'Click',  'Fermer');
            $("#sidebar").hide();
        });


        var oW = 768;
        var oH = 629;

        var cH = parseInt((wW * oH) / oW);

        // if(wW < 1023)  $(".front_talented_bg").height( cH );

        /***************/


        $(document).on('mousewheel', '.noscrollwindow',
                function (e) {
                    var delta = e.originalEvent.wheelDelta;
                    this.scrollTop += (delta < 0 ? 1 : -1) * 30;
                    e.preventDefault();
                });



        //$(".title_paragraph .field-items .field-item:last-child").addClass('last_child');




        var is_auto = 4000;

        var size_thums = $("#slider_logos li").size();
        var visible = 4;
        var speed = 1500;
        if (wW > 375 && wW < 667) {
            
            visible = 2;
        }
        if (wW < 420) {
            visible = 1;
            speed = 800;
        }
        if (size_thums <= 4){
            is_auto = 0;
           if (wW > 480)  $(".block-successstory-sub .jcarousel-control-prev, .block-successstory-sub .jcarousel-control-next").hide(); 
        }

        $(".jcarousel").jCarouselLite({
            auto: is_auto,
            speed: speed,
            visible: visible,
            btnNext: ".jcarousel-control-next",
            btnPrev: ".jcarousel-control-prev"
        });


        
        $(".jcarousel-HRpolicy").jCarouselLite({
            auto: 4000,
            speed: speed,
            visible: visible,
            btnNext: ".jcarousel-control-next",
            btnPrev: ".jcarousel-control-prev"
        });
        
        if( wW < 767 ){
            $(".jcarousel-HRpolicy").css({
               // 'width':wW - 100
            });
        }

        /*
         $(".row-wrap-testimonials").each(function(ind,el){
         var wimi = $(this).find(".field_big_image-wrapper img").width();
         $(this).find(".field_big_image-wrapper").css({'margin':'25px auto 0 auto' ,'width':wimi });
         });
         */

        var $popup = $("#popup_video_home");
        // var popup_content = $("#video_iframe").html();
        var $overlay = $('.overlay-grey');
        $(".explore_boccard,.btn_crv_rof_right,.discover_boccard_new_btn").click(function () {
            
            ga('send', 'event', 'HOME-Slider', 'Click', 'Boccard-History');
            
            var dH = $(document).height();
            
            
            var conf = {
                'height' : dH,
                'width' :'100%'
            };
            
            if( wW > 767 && wW < 1024 ) conf.width=1024;
            else conf.width='100%';
            $overlay.css(conf).show();
            

            // $popup.find('.popup_inner').prepend(popup_content);
            $popup.css({
                'display': 'block',
                'top': $(window).scrollTop() + 200,
                'left': (wW - $popup.outerWidth()) / 2
            }).show();
        });

        $(".close_popup,.overlay-grey").click(function () {
            $popup.find('iframe').remove();
            $popup.hide();
            $overlay.hide();
        });

        var first_trigger_li = '';
        var marg = Drupal.settings.custom.arg;
        first_trigger_li = $(".block-successstory-sub").attr('tit');
            
        if ( marg[2] !== undefined ) first_trigger_li = marg[2];

        $(".block-successstory-sub-" + first_trigger_li).show();
        var first_set_num = $(".slider_logos-" + first_trigger_li + " li.row-wrap-testimonials ").size();
        
        
        
        if( wW > 480 ) $(".row-wrap-testimonials .body").width(wWs-140)
        
        
        
        $(".jcarousel2").css('width',wWs);
        $(".slider_logos li.row-wrap-testimonials").css('width',wWs);

        
        $('.slider_logos-' + first_trigger_li).show().css('width', first_set_num * wWs);
       resetOtherSlidesGroups(first_trigger_li);
            $(".jcarousel-control-next2").show();
            $(".jcarousel-control-prev2").hide();
        var times = 1;
        
        $(".s1 .swiper-slide").click(function () {
            times = 1;
            var ind = $(this).attr('rel');
            var cnt_li = $(".slider_logos-" + ind + " li.row-wrap-testimonials ").size();
            $(".slider_logos-" + ind).css('width', cnt_li * wWs);
            $(".block-successstory-sub").hide();
            $(".block-successstory-sub-" + ind).show();

            $(".jcarousel-control-next2").show();
            $(".jcarousel-control-prev2").hide();

            resetOtherSlidesGroups(ind);

        });




        $(".jcarousel-control-next2").bind('click',function (e) {
            e.preventDefault();
            var ind = $(this).attr('rel');
            var cnt_li = $(".slider_logos-" + ind + " li.row-wrap-testimonials").size();
            if (times == (cnt_li - 1)) {
                $(".jcarousel-control-next2").hide();
            }
            if (cnt_li == 1)
                return;
            if (times < cnt_li) {
                if (times == cnt_li){
                    resetOtherSlidesGroups(ind);
                    return;
                }
                times++;
                $(".jcarousel-control-prev2").show();
                $(".slider_logos-" + ind).animate({'margin-left': '-='+wWs+'px'}, 800);
            }
        });
        $(".jcarousel-control-prev2").click(function (e) {
            e.preventDefault();
            var ind = $(this).attr('rel');
            var cnt_li = $(".slider_logos-" + ind + " li.row-wrap-testimonials").size();
            if (cnt_li == 1)
                return;
            if (times > 1) {
                if (times == 2) {
                    $(".jcarousel-control-prev2").hide();
                }
                if (times == 1) {
                    resetOtherSlidesGroups(ind);
                    return;
                }
                
                times--;
                $(".jcarousel-control-next2").show();
                $(".slider_logos-" + ind).animate({'margin-left': '+='+wWs+'px'}, 800);
            }
        });

        function resetOtherSlidesGroups(ind) { 
            $('.slider_logos').css('margin-left', 0);
            $(".jcarousel-control-prev2").hide();
        }



        $("#menu_wrapper .scroll-pane").height(hH - 100);

        $("#menu-toggle").on("click", function (e) {
            //$("#menu-toggle").bind('click',function(e){
            e.stopPropagation();
            $("#menu_wrapper").show().addClass('open');
            $('.scroll-pane').jScrollPane();
        });


        if (wW > 1023) {
            $("#menu-toggle").mouseover(function (e) {
                e.stopPropagation();
                $("#menu-toggle").trigger('click');
            });

            $("#menu_wrapper").mouseleave(function (e) {
                e.stopPropagation();
                $("#menu_wrapper").hide().removeClass('open');
            });

        }



        $(".custom_search_input").click(function (e) {
            e.stopPropagation();
            //$("#menu-toggle").trigger('click');
        });



        $(".close_menu,body").click(function (e) {
            e.stopPropagation();
            $("#menu_wrapper").hide().removeClass('open');
        });

        $('#menu_wrapper .nolink').next().slideUp();

        $("#menu_wrapper .nolink").click(function (e) {
            e.stopPropagation();
            
            $(this).toggleClass('active');
            $(this).next().toggle("fast").siblings("[id]").hide("fast");
            
            var mid = $(this).closest('li').attr('rel');
                
            if(     //en
                    mid==391 || mid==392 || mid==390 || mid==384 || mid==395 ||
                    //fr
                    mid==803 || mid==826 || mid==815 || mid==842 || mid==847  ||
                    //es
                    mid==890 || mid==897 || mid==894 || mid==903 || mid==907 || mid==907 || mid==896 ||
                    //cn
                    mid==1027 || mid==1033 || mid==1053 || mid==1065 || mid==1073 ||
					//pl
                    mid==1196 || mid==1199 || mid==1207 || mid==1210 || mid==1213
                    ){
                var next = $(this).next().show();
                    var allUlElements = $("ul");
                    next.find(allUlElements).show();
            }
            
        if(mid==976 || mid==799 || mid==888 || mid==1020 || mid==1193){
            //en
            var ul = $(".mid-380 ul")[0];
            $( "#menu_wrapper .mid-380" ).find( ul ).show();
            var ul = $(".mid-974 ul")[0];
            $( "#menu_wrapper .mid-974" ).find( ul ).show();
            //fr
            var ul = $(".mid-1002 ul")[0];
            $( "#menu_wrapper .mid-1002" ).find( ul ).show();
            var ul = $(".mid-1094 ul")[0];
            $( "#menu_wrapper .mid-1094" ).find( ul ).show();
            //es
            var ul = $(".mid-977 ul")[0];
            $( "#menu_wrapper .mid-977" ).find( ul ).show();
            var ul = $(".mid-978 ul")[0];
            $( "#menu_wrapper .mid-978" ).find( ul ).show();
            //cn
            var ul = $(".mid-1022 ul")[0];
            $( "#menu_wrapper .mid-1022" ).find( ul ).show();
            var ul = $(".mid-1023 ul")[0];
            $( "#menu_wrapper .mid-1023" ).find( ul ).show();
			//pl
            var ul = $(".mid-1194 ul")[0];
            $( "#menu_wrapper .mid-1194" ).find( ul ).show();
            var ul = $(".mid-1205 ul")[0];
            $( "#menu_wrapper .mid-1205" ).find( ul ).show();

        }
            /*
            if ( !$(this).hasClass('active') ) {
                $(this).next().show();
                $(this).addClass('active');
                if ($(this).text() == 'Oil & Gas' || $(this).text() == 'Nuclear' || $(this).text() == 'Power & Utilities'
                        || $(this).text() == 'Industries' || $(this).text() == 'Brewery' || $(this).text() == 'Food & Beverage'
                        || $(this).text() == 'Cosmetics & Hygiene' || $(this).text() == 'Pharma & Biotech') {
                    var next = $(this).next().show();
                    var allUlElements = $("ul");
                    next.find(allUlElements).show();
                }
            } else {
                $(this).next().hide();
                $(this).removeClass('active');
            }
            */

            $("#menu_wrapper .scroll-pane").height(hH - 100);
            $('.scroll-pane').jScrollPane();
            
        });



        $(".clearfix.rl").prev().addClass('last');


if( $('.nochpt ul li').size()  > 0 || $('.childs ul li').size()  > 0 ){
        $(".dropdown,.close_categ").click(function (e) {
            ga('send', 'event', 'Marches&Metiers', 'Click', 'SS Menu');
            if (!$("#categories-pannel").hasClass('open')) {
                e.stopPropagation();
                $("#categories-pannel").slideDown('fast').addClass('open');
                if(wW < 1024){
                    $('html, body').animate({
                    scrollTop: (( $("#categories-pannel").offset().top ) -90 )
                    }, 800);
                }
                if ($(".node-page .description").hasClass('open')) {
                    $(".node-page .description").slideUp('fast').removeClass('open');
                }
                $(".disk_open_close").addClass('nohover');
            }
            else {
                $("#categories-pannel").slideUp('fast').removeClass('open');
            }
        });
}


        $(".close_profile_4").click(function () {
            $("#profile-first-set").slideUp("fast");
            $(".disk_open_close").show();
        });

       $(".disk_open_close").click(function (e) {
            // this function is used for categories and hr-policies pages
            if ($(this).hasClass('hr-policy')) {
            ga('send', 'event', 'Temoignages', 'Click', 'Read more');
                $("#profile-first-set").slideDown('fast');
                $(this).hide();
            }
            else {
                ga('send', 'event', 'Marches&Metiers', 'Click', 'Read more');
                if ($("#categories-pannel").hasClass('open') && !$(".node-page .description").hasClass('open')) {
                    //$("#categories-pannel").slideUp('fast').removeClass('open');
                    $(this).removeClass('open');
                    return;
                }
                if (!$("#categories-pannel").hasClass('open') && !$(".node-page .description").hasClass('open')) {
                    e.stopPropagation();
                    $(".node-page .description").slideDown('fast').addClass('open');
                    $(this).addClass('open');
                    return;
                }
                if (!$("#categories-pannel").hasClass('open') && $(".node-page .description").hasClass('open')) {
                    $(".node-page .description").slideUp('fast').removeClass('open');
                    $(this).removeClass('open');
                    return;
                }
                
            }
        });

        $('.node-page .description').click(function (event) {
            $('html').one('click', function () {
                $("#categories-pannel").slideUp('fast').removeClass('open');
                $(".disk_open_close").removeClass('nohover');
                $(".node-page .description").slideUp().removeClass('open');
                $(".disk_open_close").removeClass('open');
            });
            event.stopPropagation();
        });
        
        $('#categories-pannel').click(function (event) {
            $('html').one('click', function () {
                $("#categories-pannel").slideUp('fast').removeClass('open');
                $(".disk_open_close").removeClass('nohover');
                $(".node-page .description").slideUp().removeClass('open');
                $(".disk_open_close").removeClass('open');
            });
            event.stopPropagation();
        });
        

        $(".node-page .close_desc,body").click(function (e) {
            e.stopPropagation();
            $(".node-page .description").slideUp().removeClass('open');
            $(".disk_open_close").removeClass('open');
        });
        $(".categories_inner_close_desc,body").click(function (e) {
            e.stopPropagation();
            $("#categories-pannel").slideUp('fast').removeClass('open');
            $(".disk_open_close").removeClass('nohover');
        });
        /***********************************/



        $(".testi-hr-block .jcarousel-HRpolicy li").click(function () {

            $('.yt_player_iframe').each(function () {
                this.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
            });

            var rel = $(this).attr('rel');
            $(".testi-hr-block .jcarousel-HRpolicy li").removeClass('active');
            $(this).addClass('active');
            $(".video-testimonial").hide();
            $(".video-testimonial-" + rel).show();
			$(".video-youku-testimonial").css('padding-bottom', '0');
			$(".video-youku-testimonial.video-testimonial-" + rel).css('padding-bottom', '56.25%');
        });


        $("#edit-country-company").change(function () {
            var iso2 = $(this).val();
            var url = Drupal.settings.basePath + 'ajax_get_phone_code_page/' + iso2;
            $.ajax({
                url: url,
                dataType: 'json',
                success: function (data) {
                    $("#edit-tel-code").val(data)
                }
            });
        });

        function key_exists(key, search) {
            if (!search || (search.constructor !== Array && search.constructor !== Object)) {
                return false;
            }
            for (var i = 0; i < search.length; i++) {
                if (search[i] === key) {
                    return true;
                }
            }
            return key in search;
        }


        $('#change_hidden_product').chosen();
        //$('#change_hidden_product').chosen({ allow_single_deselect: true });

        $("#edit-market").bind('change', function () {
            var userInput = $(this).val();
            var arg1 = Drupal.settings.custom.arg[1];
            var url = Drupal.settings.basePath + 'get_products_by_cat/' + (userInput != '' ? userInput : arg1);
            var html = '';
            var s = 'ma';
            $("#change_hidden_product option").each(function () {
                if ($(this).hasClass(s))
                    $(this).remove();
            });
            if (userInput || arg1) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        for (var key in data) {
                            html += '<option class="' + s + '" value="' + key + '">' + data[key] + '</option>';
                        }
                        $("#change_hidden_product").append(html);
                        $('#change_hidden_product').chosen().trigger("chosen:updated");
                        var seen = {};
                        $('#change_hidden_product').children().each(function () {
                            var txt = $(this).attr('value');
                            if (seen[txt]) {
                                $(this).remove();
                            } else {
                                seen[txt] = true;
                            }
                        });
                        $('#change_hidden_product').chosen().trigger("chosen:updated");
                    }
                });
            }

        });

        $("#edit-need").change(function () {
            var userInput = $(this).val();
            var arg1 = Drupal.settings.custom.arg[1];
            var url = Drupal.settings.basePath + 'get_products_by_cat/' + (userInput != '' ? userInput : arg1);
            var html = '';
            var s = 'ne';
            $("#change_hidden_product option").each(function () {
                if ($(this).hasClass(s))
                    $(this).remove();
            });
            if (userInput || arg1) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        for (var key in data) {
                            html += "<option class=" + s + " value=" + key + ">" + data[key] + "</option>";
                        }
                        $("#change_hidden_product").append(html);
                        $('#change_hidden_product').chosen().trigger("chosen:updated");
                        var seen = {};
                        $('#change_hidden_product').children().each(function () {
                            var txt = $(this).attr('value');
                            if (seen[txt]) {
                                $(this).remove();
                            } else {
                                seen[txt] = true;
                            }
                        });
                        $('#change_hidden_product').chosen().trigger("chosen:updated");
                    }
                });
            }

        });



        $("#change_hidden_product").change(function () {
            $('input[name="products"]').val($(this).val());
        });

      $(".custom_search_submit").click(function () {
            if (wW < 480) {
                var val = $(".custom_search_input.mobile").val();
                if(val=='') {
                    alert(Drupal.t('Search field is required'));
                    return;
                }
                window.location.href = Drupal.settings.basePath + my_lang_srch + 'search/site/' + val;
            }
        
            var anW = '84%';
            if (wW < 768) {
                anW = '84%';
            }

            if (!$(this).hasClass('open')) {
                $(".custom_search_input").show().val('').animate({width: anW}, 300);
                $(this).addClass('open');
            }
			else if($(this).hasClass('open') && $(".custom_search_input").val() == ""){
				$(".custom_search_input").animate({width: '0'}, 300, function () {
                    $(".custom_search_input").hide()
                });
                $(this).removeClass('open');
			}			
			else {
				window.location.href = Drupal.settings.basePath + my_lang_srch + 'search/site/' + $(".custom_search_input").val();
            }
        });


        $('.explore_boccard,.explore_boccard_loc').hover(function () {
            $(this).addClass('mhover');
        }, function () {
            $(this).removeClass('mhover');
        });

        $("#clicKsAll").click(function () {
            $(this).select();
        });

        $('.crad:eq(0)').addClass("check");
        $(".zradio").click(function () {
            var nlid = $(this).attr('rel');
            $('.hrad-' + nlid).prop("checked", true);
            $('.crad').removeClass("check");
            $('.crad-' + nlid).addClass("check");
            $('#categ_hidden').val(nlid);
        });
        $(".close_pop_jet").click(function () {
            $(".popup_nl_jet").hide();
        });

        $("#block-custom-front_newsletter_form .mail_submit").click(function () {
            
            ga('send', 'event', 'HOME-Join', 'Click', 'Newsletter');
            
            var email = $('.mail_input').val();
            var rgpd = $('.rgpd_input').is(':checked');
            if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                alert(Drupal.t("You have entered an invalid email address!"));
                return;
            }
            if (email == '') {
                alert(Drupal.t('Email is required'));
                return;
            }
            if (rgpd == false) {
                alert(Drupal.t('You must accept our terms and conditions'));
                return;
            }

            $(".popup_nl_jet").show().find('#mail_hidden').val(email);
        });


        $(".custom_search_input").on("keydown", function (event) {
            if (event.which == 13) {
                window.location.href = Drupal.settings.basePath + my_lang_srch + 'search/site/' + $(this).val();
            }
        });

  


        $(window).load(function () {
            
            
            
            
          
            

            /***************************/
            var market_arr = new Array();
            var need_arr = new Array();
            var arg1 = Drupal.settings.custom.arg[1];
            if (arg1 != '') {
                $("#edit-market option").each(function () {
                    market_arr.push($(this).val());
                });
                $("#edit-need option").each(function () {
                    need_arr.push($(this).val());
                });
                if (market_arr.indexOf(arg1) != -1) {
                    $("#edit-market").trigger('change');
                    $("#edit-market").val(arg1);
                    $('#change_hidden_product').chosen().trigger("chosen:updated");
                }
                if (need_arr.indexOf(arg1) != -1) {
                    $("#edit-need").trigger('change');
                    $("#edit-need").val(arg1);
                    $('#change_hidden_product').chosen().trigger("chosen:updated");
                }
                $("#edit-need option").each(function () {
                    if ($(this).val() == arg1)
                        need = arg1;
                });
            }
            
            
            
        });

        var nameAnc = '';

        var url = window.location.href, idx = url.indexOf("#");
        var nameAnc = idx != -1 ? url.substring(idx + 1) : "";
        if (nameAnc != '') {
            $('html, body').animate({
                scrollTop: $('[name="' + nameAnc + '"]').offset().top - 80
            }, 1200);
        }



//function ga(Send, event, target, click,btn ){
//    console.log(Send+' --- '+ event+' --- '+ target+' --- '+ click+' --- '+btn);
//}


/* GA */

if (typeof jQuery != 'undefined') {
   jQuery(document).ready(function($) {
           var filetypes = /\.(zip|txt|exe|pdf|doc*|xls*|ppt*|mp3)$/i;
           var baseHref = '';
           if (jQuery('base').attr('href') != undefined) baseHref = jQuery('base').attr('href');
           jQuery('a').each(function() {
                   if(!$(this).hasClass("no_auto_download")) {
                          var href = jQuery(this).attr('href');
                          if (href && (href.match(/^https?\:/i)) && (!href.match(document.domain))) {
                                  jQuery(this).click(function() {
                                         var extLink = href.replace(/^https?\:\/\//i, '');
                                     /* _gaq.push(['_trackEvent', 'External', 'Click', extLink]);*/
                                         ga('send', 'event', 'External', 'Click', extLink, 1);
                                         if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
                                                 setTimeout(function() { location.href = href; }, 200);
                                                 return false;
                                         }
                                  });
                          }
                          else if (href && href.match(/^mailto\:/i)) {
                                  jQuery(this).click(function() {
                                         var mailLink = href.replace(/^mailto\:/i, '');
                                         /*_gaq.push(['_trackEvent', 'Email', 'Click', mailLink]);*/
                                         ga('send', 'event', 'Email', 'Click', mailLink, 1);
                                  });
                          }
                          else if (href && href.match(filetypes)) {
                                  jQuery(this).click(function() {
                                         var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
                                         var filePath = href;
                                         /*_gaq.push(['_trackEvent', 'Download', 'Click-' + extension, filePath]);*/
                                         ga('send', 'event', 'Download', 'Click-'+ extension, filePath, 1);
                                         if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
                                                 setTimeout(function() { location.href = baseHref + href; }, 200);
                                                 return false;
                                         }
                                  });
                          }
                   }
           });
   });
}

/*
         * Smootscroll
         */
        $('.smoothScroll').on('click', function() { // Au clic sur un élément
            var page = $(this).attr('href'); // Page cible
            var speed = 750; // Durée de l'animation (en ms)
            $('html, body').animate( { scrollTop: $(page).offset().top - 220 }, speed ); // Go
            return false;
        });










    });
})(jQuery);
