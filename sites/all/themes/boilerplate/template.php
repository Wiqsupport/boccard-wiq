<?php
function boilerplate_apachesolr_search_noresults(){
  return '<ul class="nores_solr">
  <li>'.t('Check if your spelling is correct, or try removing filters.').'</li>
  <li>'.t('Remove quotes around phrases to match each word individually: <em>"blue drop"</em> will match less than <em>blue drop</em>.').'</li>
  <li>'.t('You can require or exclude terms using + and -: <em>big +blue drop</em> will require a match on <em>blue</em> while <em>big blue -drop</em> will exclude results that contain <em>drop</em>.').'</li>
  </ul>';
}
function boilerplate_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('« ')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('< ')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t(' >')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t(' »')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}
function boilerplate_preprocess_search_results(&$vars) {
  $itemsPerPage = 10;
  $currentPage = (isset($_REQUEST['page']) ? $_REQUEST['page'] : 0) + 1;
  $total = $GLOBALS['pager_total_items'][0];  // Determine which results are being shown ("Showing results x through y")
  $start = (10 * $currentPage) - 9;  // shown on the page. This prevents things like "Displaying 11-20 of 17".
  $end = (($itemsPerPage * $currentPage) >= $total) ? $total : ($itemsPerPage * $currentPage);  // If there is more than one page of results:
  if ($total > $itemsPerPage) {
    $vars['search_totals'] = t('Displaying !start - !end of !total results', array(
      '!start' => $start,
      '!end' => $end,
      '!total' => $total,
    ));
  }
  else {
    $vars['search_totals'] = t('Displaying !total !results_label', array(
      '!total' => $total,
      '!results_label' => format_plural($total, 'result', 'results'),
    ));
  }
}

function boilerplate_js_alter(&$javascript){
    $jquery_path = drupal_get_path('theme','boilerplate') . '/js/jquery-1.11.3.min.js';
    $javascript[$jquery_path] = $javascript['misc/jquery.js']; 
    $javascript[$jquery_path]['version'] = '1.11.3'; 
    $javascript[$jquery_path]['data'] = $jquery_path; 
    unset($javascript['misc/jquery.js']);
}

/*
 * Here we override the default HTML output of drupal.
 * refer to http://drupal.org/node/550722
 */

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('clear_registry')) {
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();
}
// Add Zen Tabs styles
if (theme_get_setting('boilerplate_tabs')) {
  drupal_add_css( drupal_get_path('theme', 'boilerplate') . '/css/tabs.css');
}

/**
 * Preprocesses the wrapping HTML.
 *
 * @param array &$variables
 *   Template variables.
 */
function boilerplate_preprocess_html(&$vars) {
  if (!module_exists('conditional_styles')) {
    boilerplate_add_conditional_styles();
  }
  
    global $user;
  if( arg(0) == 'node' && in_array( boilerplate_node_type(arg(1)) , array( 'news' , 'press' ) ) || $user->uid <> 0 ) {
    $meta_robots = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'content' =>  'noarchive, nofollow, noindex, noodp, nosnippet',
        'name' => 'robots',
      )
    );
    drupal_add_html_head($meta_robots, 'meta_robots');
  }
  
  // Setup IE meta tag to force IE rendering mode
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' =>  'IE=edge,chrome=1',
      'http-equiv' => 'X-UA-Compatible',
    )
  );
  //  Mobile viewport optimized: h5bp.com/viewport
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' =>  'width=device-width, initial-scale=1',
      'name' => 'viewport',
    )
  );
  
  if( arg(0)=='news' ){
    $meta_og_image = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'content' =>  'http://www.boccard.com/sites/all/themes/boilerplate/images/og_logo.jpg',
        'name' => 'og:image',
      )
    );
    drupal_add_html_head($meta_og_image, 'meta_og_image');
  }
  

  
  //<meta name="" content="" />
  
  
  // Add header meta tag for IE to head
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_viewport, 'meta_viewport');
}

function boilerplate_node_type($nid){
    $q = db_select('node', 'n');
    $q->fields('n', array('type'));
    $q->condition('n.nid', $nid );
    $res = $q->execute()->fetchField();
    return $res;
}

function boilerplate_preprocess_page(&$vars, $hook) {
  global $language;
  $lang_name=$language->language;

  $lang_cook=$lang_name;
  if($lang_name <> 'fr') $lang_cook='en';

// if(arg(0)=='history'){
//   drupal_add_js(drupal_get_path('theme', 'boilerplate') . '/js/jquery-migrate-1.2.1.min.js');
//
//   drupal_add_css(drupal_get_path('theme', 'boilerplate') . '/slick/slick.css');
//   drupal_add_css(drupal_get_path('theme', 'boilerplate') . '/slick/slick-theme.css');
//   drupal_add_css(drupal_get_path('theme', 'boilerplate') . '/slick/my_style.css');
//   drupal_add_js(drupal_get_path('theme', 'boilerplate') . '/js/fisheye-iutil.min.js');
//   drupal_add_js(drupal_get_path('theme', 'boilerplate') . '/js/jquery.jqDock.min.js');
//   drupal_add_js(drupal_get_path('theme', 'boilerplate') . '/slick/slick.min.js');
//   drupal_add_js(drupal_get_path('theme', 'boilerplate') . '/js/timeline.js');
// }
       
  drupal_add_js('jQuery.extend(Drupal.settings, { "pathToTheme": "' . base_path().path_to_theme() . '" });', 'inline');
  drupal_add_css(path_to_theme(). '/css/chosen.css',  array('weight' => 992));
  drupal_add_css(path_to_theme(). '/css/swiper.min.css',  array('weight' => 993));
  drupal_add_css(path_to_theme(). '/css/jcarousel.responsive.css',  array('weight' => 994));
  drupal_add_css(path_to_theme(). '/css/carousel.css',  array('weight' => 995));
  drupal_add_css(path_to_theme(). '/css/jquery.jscrollpane.css',  array('weight' => 996));

  drupal_add_css(path_to_theme(). '/css/style.css',  array('weight' => 997));
  drupal_add_css(path_to_theme(). '/css/media_style.css',  array('weight' => 998));

  if ($lang_name=='fr') drupal_add_css(path_to_theme(). '/css/style-fr.css',  array('weight' => 999));
  if ($lang_name=='es') drupal_add_css(path_to_theme(). '/css/style-es.css',  array('weight' => 999));
  if ($lang_name=='zh-hans') drupal_add_css(path_to_theme(). '/css/style-zh-hans.css',  array('weight' => 999));
  if ($lang_name=='pl') drupal_add_css(path_to_theme(). '/css/style-pl.css',  array('weight' => 999));
  if ($lang_name=='ru') drupal_add_css(path_to_theme(). '/css/style-ru.css',  array('weight' => 999));
       // $vars['styles'] = drupal_get_css();
  
        
  
  if (isset($vars['node_title'])) {
    $vars['title'] = $vars['node_title'];
  }
  // Adding a class to #page in wireframe mode
  if (theme_get_setting('wireframe_mode')) {
    $vars['classes_array'][] = 'wireframe-mode';
  }
  // Adding classes wether #navigation is here or not
  if (!empty($vars['main_menu']) or !empty($vars['sub_menu'])) {
    $vars['classes_array'][] = 'with-navigation';
  }
  if (!empty($vars['secondary_menu'])) {
    $vars['classes_array'][] = 'with-subnav';
  }

  
}

function boilerplate_preprocess_node(&$vars) {
  // Add a striping class.
  $vars['classes_array'][] = 'node-' . $vars['zebra'];
}

function boilerplate_preprocess_block(&$vars, $hook) {
  // Add a striping class.
  $vars['classes_array'][] = 'block-' . $vars['zebra'];
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function boilerplate_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('boilerplate_breadcrumb');
  if ($show_breadcrumb == 'yes' || ($show_breadcrumb == 'admin' && arg(0) == 'admin')) {



    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('boilerplate_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }
    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('boilerplate_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('boilerplate_breadcrumb_title')) {
        $item = menu_get_item();
        if (!empty($item['tab_parent'])) {
          // If we are on a non-default tab, use the tab's title.
          $title = check_plain($item['title']);
        }
        else {
          $title = drupal_get_title();
        }
        if ($title) {
          $trailing_separator = $breadcrumb_separator;
        }
      }
      elseif (theme_get_setting('boilerplate_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }
      // Provide a navigational heading to give context for breadcrumb links to
      // screen-reader users. Make the heading invisible with .element-invisible.
      $heading = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

      return $heading . '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . $trailing_separator . $title . '</div>';
    }
  }
  // Otherwise, return an empty string.
  return '';
}

/**
 * Adds conditional CSS from the .info file.
 *
 * Copy of conditional_styles_preprocess_html().
 */
function boilerplate_add_conditional_styles() {
  // Make a list of base themes and the current theme.
  $themes = $GLOBALS['base_theme_info'];
  $themes[] = $GLOBALS['theme_info'];
  foreach (array_keys($themes) as $key) {
    $theme_path = dirname($themes[$key]->filename) . '/';
    if (isset($themes[$key]->info['stylesheets-conditional'])) {
      foreach (array_keys($themes[$key]->info['stylesheets-conditional']) as $condition) {
        foreach (array_keys($themes[$key]->info['stylesheets-conditional'][$condition]) as $media) {
          foreach ($themes[$key]->info['stylesheets-conditional'][$condition][$media] as $stylesheet) {
            // Add each conditional stylesheet.
            drupal_add_css(
              $theme_path . $stylesheet,
              array(
                'group' => CSS_THEME,
                'browsers' => array(
                  'IE' => $condition,
                  '!IE' => FALSE,
                ),
                'every_page' => TRUE,
              )
            );
          }
        }
      }
    }
  }
}


/**
 * Generate the HTML output for a menu link and submenu.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @return
 *   A themed HTML string.
 *
 * @ingroup themeable
 */

function boilerplate_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  // Adding a class depending on the TITLE of the link (not constant)
  $element['#attributes']['class'][] = drupal_html_class($element['#title']);
  // Adding a class depending on the ID of the link (constant)
  if (isset($element['#original_link']['mlid']) && !empty($element['#original_link']['mlid'])) {
    $element['#attributes']['class'][] = 'mid-' . $element['#original_link']['mlid'];
    $element['#attributes']['rel'] = $element['#original_link']['mlid'];
  }
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Override or insert variables into theme_menu_local_task().
 */
function boilerplate_preprocess_menu_local_task(&$variables) {
  $link =& $variables['element']['#link'];

  // If the link does not contain HTML already, check_plain() it now.
  // After we set 'html'=TRUE the link will not be sanitized by l().
  if (empty($link['localized_options']['html'])) {
    $link['title'] = check_plain($link['title']);
  }
  $link['localized_options']['html'] = TRUE;
  $link['title'] = '<span class="tab">' . $link['title'] . '</span>';
}

/*
 *  Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */

function boilerplate_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;

}
